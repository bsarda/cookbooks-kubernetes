# Kubectl autocomplete bash

Enable kubectl command completion using tab (on Linux)

## Install

```sh
sudo apt install -y bash-completion
echo "source <(kubectl completion bash)" >> ~/.bashrc
source ~/.bashrc
```

## Result

kubectl get [tab]

```sh
kubectl create [tab]
clusterrole          configmap            deployment           namespace            priorityclass        role                 secret               serviceaccount       
clusterrolebinding   cronjob              job                  poddisruptionbudget  quota                rolebinding          service              
```