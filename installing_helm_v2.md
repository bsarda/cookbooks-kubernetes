# Installing Helm

## Install client

First step is to install the client. Choose the version with the env variable

`HELM_VER=2.14.3`

```shell
wget https://get.helm.sh/helm-v${HELM_VER}-linux-amd64.tar.gz


    --2019-08-30 14:07:56--  https://get.helm.sh/helm-v2.14.3-linux-amd64.tar.gz
    Resolving get.helm.sh (get.helm.sh)... 152.199.20.79, 2606:2800:233:b40:171d:1a2f:2077:f6b
    Connecting to get.helm.sh (get.helm.sh)|152.199.20.79|:443... connected.
    HTTP request sent, awaiting response... 200 OK
    Length: 26533763 (25M) [application/x-tar]
    Saving to: ‘helm-v2.14.3-linux-amd64.tar.gz’

    helm-v2.14.3-linux-amd64.tar.gz                    100%[==============================================================================================================>]  25,30M  94,2MB/s    in 0,3s
    2019-08-30 14:07:57 (94,2 MB/s) - ‘helm-v2.14.3-linux-amd64.tar.gz’ saved [26533763/26533763]
```


Then untar it

```shell
tar -xvzf helm-v${HELM_VER}-linux-amd64.tar.gz -C /tmp


    linux-amd64/
    linux-amd64/helm
    linux-amd64/README.md
    linux-amd64/LICENSE
    linux-amd64/tiller
```

move executable to the bin folder

```shell
sudo mv /tmp/linux-amd64/helm /usr/local/bin/helm
sudo mv /tmp/linux-amd64/tiller /usr/local/bin/tiller
```

## Get cluster credential (PKS)

Optional - clean the existing tokens to avoid multi-cluster confusion

```shell
rm .kube/config
```

Get the creds for your cluster  
name of the cluster: `CLUSTER_NAME=kn2`

```shell
pks get-credentials ${CLUSTER_NAME}


    Fetching credentials for cluster kn2.
    Password: ********
    Context set for cluster kn2.

    You can now switch between clusters by using:
    $kubectl config use-context <cluster-name>
```

Switch to that cluster

```shell
kubectl config use-context ${CLUSTER_NAME}


    Switched to context "kn2".
```


## Install helm in the cluster

Check your token is still valid

```shell
kubectl get no


    NAME                                   STATUS   ROLES    AGE   VERSION  
    0df01c98-ae33-4cc3-bfc4-674dd88a8669   Ready    <none>   14h   v1.13.5  
    3bf1f3d4-e005-4f89-a7f4-4829f392130d   Ready    <none>   14h   v1.13.5  
    4acf7ed6-bc26-490e-a960-bd30c0d15b4a   Ready    <none>   14h   v1.13.5  
```

Init helm and install tiller + helm

```shell
helm init


    $HELM_HOME has been configured at /home/ubuntu/.helm.

    Tiller (the Helm server-side component) has been installed into your Kubernetes Cluster.

    Please note: by default, Tiller is deployed with an insecure 'allow unauthenticated users' policy.
    To prevent this, run `helm init` with the --tiller-tls-verify flag.
    For more information on securing your installation see: https://docs.helm.sh/using_helm/#securing-your-helm-installation
```


## Check the status

The deployment and replicaset should exists

```shell
kubectl get all -n kube-system


    NAME                                        READY   STATUS    RESTARTS   AGE
    pod/coredns-54586579f6-7hjc5                1/1     Running   0          14h
    pod/coredns-54586579f6-955df                1/1     Running   0          14h
    pod/coredns-54586579f6-nf99l                1/1     Running   0          14h
    pod/kubernetes-dashboard-6c68548bc9-tptwd   1/1     Running   0          14h
    pod/metrics-server-5475446b7f-hx5s4         1/1     Running   0          14h
    pod/tiller-deploy-57f498469-g4n5x           1/1     Running   0          40s

    NAME                           TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)         AGE
    service/kube-dns               ClusterIP   10.100.200.2    <none>        53/UDP,53/TCP   14h
    service/kubernetes-dashboard   NodePort    10.100.200.3    <none>        443:31039/TCP   14h
    service/metrics-server         ClusterIP   10.100.200.50   <none>        443/TCP         14h
    service/tiller-deploy          ClusterIP   10.100.200.96   <none>        44134/TCP       40s

    NAME                                   READY   UP-TO-DATE   AVAILABLE   AGE
    deployment.apps/coredns                3/3     3            3           14h
    deployment.apps/kubernetes-dashboard   1/1     1            1           14h
    deployment.apps/metrics-server         1/1     1            1           14h
    deployment.apps/tiller-deploy          1/1     1            1           40s

    NAME                                              DESIRED   CURRENT   READY   AGE
    replicaset.apps/coredns-54586579f6                3         3         3       14h
    replicaset.apps/kubernetes-dashboard-6c68548bc9   1         1         1       14h
    replicaset.apps/metrics-server-5475446b7f         1         1         1       14h
    replicaset.apps/tiller-deploy-57f498469           1         1         1       40s
```

The deployment have those kind of params

```shell
kubectl describe deployment -n kube-system tiller-deploy


    Name:                   tiller-deploy
    Namespace:              kube-system
    CreationTimestamp:      Fri, 30 Aug 2019 14:17:29 +0200
    Labels:                 app=helm
                            name=tiller
    Annotations:            deployment.kubernetes.io/revision: 1
    Selector:               app=helm,name=tiller
    Replicas:               1 desired | 1 updated | 1 total | 0 available | 1 unavailable
    StrategyType:           RollingUpdate
    MinReadySeconds:        0
    RollingUpdateStrategy:  1 max unavailable, 1 max surge
    Pod Template:
      Labels:  app=helm
              name=tiller
      Containers:
      tiller:
        Image:       gcr.io/kubernetes-helm/tiller:v2.14.3
        Ports:       44134/TCP, 44135/TCP
        Host Ports:  0/TCP, 0/TCP
        Liveness:    http-get http://:44135/liveness delay=1s timeout=1s period=10s #success=1 #failure=3
        Readiness:   http-get http://:44135/readiness delay=1s timeout=1s period=10s #success=1 #failure=3
        Environment:
          TILLER_NAMESPACE:    kube-system
          TILLER_HISTORY_MAX:  0
        Mounts:                <none>
      Volumes:                 <none>
    Conditions:
      Type           Status  Reason
      ----           ------  ------
      Available      True    MinimumReplicasAvailable
    OldReplicaSets:  <none>
    NewReplicaSet:   tiller-deploy-57f498469 (1/1 replicas created)
    Events:
      Type    Reason             Age   From                   Message
      ----    ------             ----  ----                   -------
      Normal  ScalingReplicaSet  15s   deployment-controller  Scaled up replica set tiller-deploy-57f498469 to 1
```

Finally, a version command should indicate client (first line) and server (second line) versions.

```shell
helm version


    Client: &version.Version{SemVer:"v2.14.3", GitCommit:"0e7f3b6637f7af8fcfddb3d2941fcc7cbebb0085", GitTreeState:"clean"}
    Server: &version.Version{SemVer:"v2.14.3", GitCommit:"0e7f3b6637f7af8fcfddb3d2941fcc7cbebb0085", GitTreeState:"clean"}
```
