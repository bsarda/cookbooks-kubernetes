# Create PKS cluster

## Env variables

```sh
CLUSTER_NAME=bsa01
```

## Get clusters

List the existing clusters:

```sh
pks clusters
```

Show an existing cluster detail

```sh
pks cluster ${CLUSTER_NAME}
```

## Create the cluster

Deploy the plan "small" with 3 Nodes

```sh
pks create-cluster ${CLUSTER_NAME} --external-hostname ${CLUSTER_NAME} --plan small --num-nodes 3
```

Wait until it's deployed

## Get status and wait

Get the cluster credentials to login on kubectl

```sh
pks get-credentials ${CLUSTER_NAME}
kubectl config use-context ${CLUSTER_NAME}
```
