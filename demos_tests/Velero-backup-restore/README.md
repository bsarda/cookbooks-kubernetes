# Install Velero on a PKS cluster without internet access

## Prepare PKS environment and env

### Image repo

Login on Harbor, then create the repository. Select **public** repo to avoid installer to login the registry.

On the repo config, select to automatically scan the images when pushed.

### K8s cluster admission plugins

The cluster's plan must have the options:

- ‘Allow Privileged’ (previously called ‘Enable Privileged Containers‘)
- ‘DenyEscalatingExec’

### Prepare the env vars

Here I use env vars to avoid typing (sysadmin lazyness :) )  
Create the vars, according to your env:

```ini
REGISTRY_HOST=harbor.corp.local
REGISTRY_PROJECT=public-repo
VELERO_VER=v1.1.0
```

### Avoid sudo for docker commands

Refer to (post-install for Linux)[https://docs.docker.com/install/linux/linux-postinstall/]

```shell
sudo groupadd docker
sudo usermod -aG docker $USER
newgrp docker
sudo chown "${USER}":"${USER}" /home/"${USER}"/.docker -R
sudo chmod g+rwx "$HOME/.docker" -R
```

## Get the CLI

Download the correct version of the CLI.  
Here, the test was done with v1.1.0

```shell
wget https://github.com/vmware-tanzu/velero/releases/download/${VELERO_VER}/velero-${VELERO_VER}-linux-amd64.tar.gz
```

THen extract it and go to foler

```shell
tar xvzf velero-${VELERO_VER}-linux-amd64.tar.gz
cd velero-${VELERO_VER}-linux-amd64
```

## Get docker image locally

:warning: If plan to use Minio, don't break internet access yet - there is additionnal images to download locally.

### From a workstation with access to internet and to Harbor

```shell
docker pull gcr.io/heptio-images/velero:${VELERO_VER}
docker tag gcr.io/heptio-images/velero:${VELERO_VER} ${REGISTRY_HOST}/${REGISTRY_PROJECT}/velero:${VELERO_VER}
docker push ${REGISTRY_HOST}/${REGISTRY_PROJECT}/velero:${VELERO_VER}
```

We will need also the restore helper

```shell
docker pull gcr.io/heptio-images/velero-restic-restore-helper:${VELERO_VER}
docker tag gcr.io/heptio-images/velero-restic-restore-helper:${VELERO_VER} ${REGISTRY_HOST}/${REGISTRY_PROJECT}/velero-restic-restore-helper:${VELERO_VER}
docker push ${REGISTRY_HOST}/${REGISTRY_PROJECT}/velero-restic-restore-helper:${VELERO_VER}
```

### From a totally out-of-band / air-gap network (manual)

If there is not a such workstation with accessibility, pull images, save them

```shell
docker save gcr.io/heptio-images/velero:${VELERO_VER} -o velero.${VELERO_VER}.tar
docker save gcr.io/heptio-images/velero-restic-restore-helper:${VELERO_VER} -o velero-restic-restore-helper.${VELERO_VER}.tar
```

Transfer the files. Then on the target workstation, import them and re-name as originally

```console
ubuntu@lab1-ubu:~$ sudo docker import velero.tar
sha256:9558be653b1cda9c61548175ad1da5b41f8525699af36caf58087adfd15c7445
ubuntu@lab1-ubu:~$ sudo docker images
REPOSITORY                                                   TAG                 IMAGE ID            CREATED             SIZE
<none>                                                       <none>              9558be653b1c        38 seconds ago      163MB
ubuntu@lab1-ubu:~$ sudo docker tag 9558be653b1c velero:${VELERO_VER}
ubuntu@lab1-ubu:~$ sudo docker images
REPOSITORY                                                   TAG                 IMAGE ID            CREATED             SIZE
velero                                                       v1.1.0              9558be653b1c        7 minutes ago       163MB
```

Do that for all the images (velero, velero-restic-restore-helper, minio, mc, nginx)  

### From a totally out-of-band / air-gap network (automated)

```shell
imageid=$(docker import velero.${VELERO_VER}.tar | awk -F':' '{print $2}' | cut -c1-12)
docker tag ${imageid} velero:${VELERO_VER}
docker tag ${imageid} ${REGISTRY_HOST}/${REGISTRY_PROJECT}/velero:${VELERO_VER}
imageid=$(docker import velero-restic-restore-helper.${VELERO_VER}.tar | awk -F':' '{print $2}' | cut -c1-12)
docker tag ${imageid} velero-restic-restore-helper:${VELERO_VER}
docker tag ${imageid} ${REGISTRY_HOST}/${REGISTRY_PROJECT}/velero-restic-restore-helper:${VELERO_VER}
imageid=$(docker import minio.tar | awk -F':' '{print $2}' | cut -c1-12)
docker tag ${imageid} minio:latest
docker tag ${imageid} ${REGISTRY_HOST}/${REGISTRY_PROJECT}/minio:latest
imageid=$(docker import mc.tar | awk -F':' '{print $2}' | cut -c1-12)
docker tag ${imageid} mc:latest
docker tag ${imageid} ${REGISTRY_HOST}/${REGISTRY_PROJECT}/mc:latest
```

## Create the credentials file for S3 endpoint

Create the creds file for s3 endpoint

```shell
cat << EOF > ~/credentials-velero
[default]
aws_access_key_id = minio
aws_secret_access_key = minio123
EOF
```

Note: _those are the default of minio quick install_

## MinioS3 install

### Pull/push from public to private

when you downloaded velero, the minio sample is included.  
Need to pull/push locally the minio image:

```shell
sudo docker pull minio/minio:latest
sudo docker tag minio/minio:latest ${REGISTRY_HOST}/${REGISTRY_PROJECT}/minio:latest
sudo docker push ${REGISTRY_HOST}/${REGISTRY_PROJECT}/minio:latest
sudo docker pull minio/mc:latest
sudo docker tag minio/mc:latest ${REGISTRY_HOST}/${REGISTRY_PROJECT}/mc:latest
sudo docker push ${REGISTRY_HOST}/${REGISTRY_PROJECT}/mc:latest
```

### Adapt the example yaml (manual)

edit the example file to point to the new repo

```shell
vim examples/minio/00-minio-deployment.yaml
```

replace `image: minio/minio:latest` by `image: harbor.corp.local/public-repo/minio:latest`  
replace `image: minio/mc:latest` by `image: harbor.corp.local/public-repo/mc:latest`  

change the exposition to **type: NodePort**

### Adapt the example yaml (automated)

The commandline to do so is:

```shell
sed -i "s@image: minio/@image: ${REGISTRY_HOST}/${REGISTRY_PROJECT}/@" examples/minio/00-minio-deployment.yaml
sed -i "s@type: ClusterIP@type: NodePort@" examples/minio/00-minio-deployment.yaml
```

### Deploy Minio

Launch the deployment of Minio

```console
ubuntu@lab1-ubu:~/velero-v1.1.0-linux-amd64$ kubectl apply -f examples/minio/00-minio-deployment.yaml
namespace/velero created
deployment.apps/minio created
service/minio created
job.batch/minio-setup created
```

### Get Minio runtime url (manual)

Check that minio is available on an external url

```console
ubuntu@lab1-ubu:~/velero-v1.1.0-linux-amd64$ kubectl get pods -n velero
NAME                    READY   STATUS      RESTARTS   AGE
minio-d9848974d-zl6pz   1/1     Running     0          59s
minio-setup-ph56h       0/1     Completed   1          59s
ubuntu@lab1-ubu:~/velero-v1.1.0-linux-amd64$ kubectl describe pod minio-d9848974d-zl6pz -n velero | grep -i Node:
Node:               e0d2041a-a5b8-448a-8566-afbde0c58844/5.197.0.6
ubuntu@lab1-ubu:~/velero-v1.1.0-linux-amd64$ kubectl get svc -n velero
NAME    TYPE       CLUSTER-IP      EXTERNAL-IP   PORT(S)          AGE
minio   NodePort   10.100.200.92   <none>        9000:32490/TCP   5m11s
```

Here, the important infos are **5.197.0.6** (IP address of the worker node which runs the minio) and **32490** (nodeport). Record that as variable

```shell
miniourl=http://5.197.0.6:32490
```

Browse on that address (http://5.197.0.6:32490)[http://5.197.0.6:32490], it should show the login ui.  
By default, login is _minio_, password is _minio123_

### Get Minio runtime url (automated)

Or, as a one liner:

```shell
minioip=$(kubectl describe pod $(kubectl get pods -n velero | grep unning | awk '{print $1}') -n velero | grep -i Node: | awk -F'/' '{print $2}')
minioport=$(kubectl get svc -n velero | grep minio | awk '{print $5}' | awk -F':' '{print $2}' | awk -F'/' '{print $1}')
miniourl="http://${minioip}:${minioport}"
echo "Minio URL is: ${miniourl}"
```

## Install velero

Bucket within Minio (integrated example) is named velero, was created on deploy time of minio. Adapt as needed.  
Provider AWS means that we'll use S3. The snapshots are not yet fully implemented, so disable them.  
Point the private registry with the `--image` option, and select to use restic as the "generic" provider by `--use-restic`.  
On the `--backup-location-config` specify the "publicUrl" for accessing Mionio from outside of this deployed cluster.  

:warning: be sure to have all the completed infos/var in order to proceed.

```console
ubuntu@lab1-ubu:~/velero-v1.1.0-linux-amd64$ velero install --provider aws --bucket velero --secret-file ~/credentials-velero --use-volume-snapshots=false --image=${REGISTRY_HOST}/${REGISTRY_PROJECT}/velero:${VELERO_VER} --use-restic --backup-location-config region=minio,s3ForcePathStyle="true",s3Url=http://minio.velero.svc:9000,publicUrl=${miniourl}
CustomResourceDefinition/backups.velero.io: attempting to create resource
CustomResourceDefinition/backups.velero.io: created
CustomResourceDefinition/backupstoragelocations.velero.io: attempting to create resource
CustomResourceDefinition/backupstoragelocations.velero.io: created
CustomResourceDefinition/deletebackuprequests.velero.io: attempting to create resource
CustomResourceDefinition/deletebackuprequests.velero.io: created
CustomResourceDefinition/downloadrequests.velero.io: attempting to create resource
CustomResourceDefinition/downloadrequests.velero.io: created
CustomResourceDefinition/podvolumebackups.velero.io: attempting to create resource
CustomResourceDefinition/podvolumebackups.velero.io: created
CustomResourceDefinition/podvolumerestores.velero.io: attempting to create resource
CustomResourceDefinition/podvolumerestores.velero.io: created
CustomResourceDefinition/resticrepositories.velero.io: attempting to create resource
CustomResourceDefinition/resticrepositories.velero.io: created
CustomResourceDefinition/restores.velero.io: attempting to create resource
CustomResourceDefinition/restores.velero.io: created
CustomResourceDefinition/schedules.velero.io: attempting to create resource
CustomResourceDefinition/schedules.velero.io: created
CustomResourceDefinition/serverstatusrequests.velero.io: attempting to create resource
CustomResourceDefinition/serverstatusrequests.velero.io: created
CustomResourceDefinition/volumesnapshotlocations.velero.io: attempting to create resource
CustomResourceDefinition/volumesnapshotlocations.velero.io: created
Waiting for resources to be ready in cluster...
Namespace/velero: attempting to create resource
Namespace/velero: already exists, proceeding
Namespace/velero: created
ClusterRoleBinding/velero: attempting to create resource
ClusterRoleBinding/velero: created
ServiceAccount/velero: attempting to create resource
ServiceAccount/velero: created
Secret/cloud-credentials: attempting to create resource
Secret/cloud-credentials: created
BackupStorageLocation/default: attempting to create resource
BackupStorageLocation/default: created
Deployment/velero: attempting to create resource
Deployment/velero: created
DaemonSet/restic: attempting to create resource
DaemonSet/restic: created
Velero is installed! ⛵ Use 'kubectl logs deployment/velero -n velero' to view the status.
```

## Tune up restic

Install is finished, but if you do a `kubectl -n velero get pods` it will show the restic pods can't succeed.

```console
ubuntu@lab1-ubu:~/velero-v1.1.0-linux-amd64$ kubectl -n velero get pods
NAME                     READY   STATUS             RESTARTS   AGE
minio-d9848974d-s6cww    1/1     Running            0          12m
minio-setup-l28rs        0/1     Completed          0          12m
restic-cgdxz             0/1     CrashLoopBackOff   5          4m42s
restic-ctd8w             0/1     CrashLoopBackOff   5          4m42s
restic-l2668             0/1     CrashLoopBackOff   5          4m42s
velero-b777b58c8-xgtcp   1/1     Running            0          4m42s

ubuntu@lab1-ubu:~/velero-v1.1.0-linux-amd64$ kubectl -n velero describe pod/restic-cgdxz | grep Events -A10
Events:
  Type     Reason     Age                    From                                           Message
  ----     ------     ----                   ----                                           -------
  Normal   Scheduled  5m9s                   default-scheduler                              Successfully assigned velero/restic-cgdxz to 48047b7a-2e1e-4b4d-8f0a-01d10f957c66
  Normal   Pulling    4m59s                  kubelet, 48047b7a-2e1e-4b4d-8f0a-01d10f957c66  Pulling image "harbor.corp.local/public-repo/velero:v1.1.0"
  Normal   Pulled     4m33s                  kubelet, 48047b7a-2e1e-4b4d-8f0a-01d10f957c66  Successfully pulled image "harbor.corp.local/public-repo/velero:v1.1.0"
  Normal   Created    3m10s (x5 over 4m30s)  kubelet, 48047b7a-2e1e-4b4d-8f0a-01d10f957c66  Created container restic
  Warning  Failed     3m10s (x5 over 4m29s)  kubelet, 48047b7a-2e1e-4b4d-8f0a-01d10f957c66  Error: failed to start container "restic": Error response from daemon: linux mounts: path /var/lib/kubelet/pods is mounted on / but it is not a shared or slave mount
  Normal   Pulled     3m10s (x4 over 4m28s)  kubelet, 48047b7a-2e1e-4b4d-8f0a-01d10f957c66  Container image "harbor.corp.local/public-repo/velero:v1.1.0" already present on machine
  Warning  BackOff    2m25s (x9 over 4m6s)   kubelet, 48047b7a-2e1e-4b4d-8f0a-01d10f957c66  Back-off restarting failed container
ubuntu@lab1-ubu:~/velero-v1.1.0-linux-amd64$
```

for that, find the daemonset of restic to edit it

```console
ubuntu@lab1-ubu:~/velero-v1.1.0-linux-amd64$ kubectl get ds --all-namespaces
NAMESPACE    NAME             DESIRED   CURRENT   READY   UP-TO-DATE   AVAILABLE   NODE SELECTOR   AGE
pks-system   fluent-bit       3         3         3       3            3           <none>          35m
pks-system   telegraf         3         3         3       3            3           <none>          35m
pks-system   vrops-cadvisor   3         3         3       3            3           <none>          26m
velero       restic           3         3         0       3            0           <none>          7m42s
```

Edit the daemonset to change the hostPath

```shell
kubectl edit ds restic -n velero
```

Change from `hostPath: path: /var/lib/kubelet/pods` to `hostPath: path: /var/vcap/data/kubelet/pods`. Save and quit (:x with vi)

With that change, the restic pods will be killed and restarted, and everything should be okay now.

```console
ubuntu@lab1-ubu:~/velero-v1.1.0-linux-amd64$ kubectl get all -n velero
NAME                         READY   STATUS      RESTARTS   AGE
pod/minio-d9848974d-s6cww    1/1     Running     0          23m
pod/minio-setup-l28rs        0/1     Completed   0          23m
pod/restic-klxmc             1/1     Running     0          53s
pod/restic-szpbq             1/1     Running     0          53s
pod/restic-wkfxv             1/1     Running     0          53s
pod/velero-b777b58c8-xgtcp   1/1     Running     0          14m

NAME            TYPE       CLUSTER-IP      EXTERNAL-IP   PORT(S)          AGE
service/minio   NodePort   10.100.200.92   <none>        9000:32490/TCP   23m

NAME                    DESIRED   CURRENT   READY   UP-TO-DATE   AVAILABLE   NODE SELECTOR   AGE
daemonset.apps/restic   3         3         3       3            3           <none>          14m

NAME                     READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/minio    1/1     1            1           23m
deployment.apps/velero   1/1     1            1           14m

NAME                               DESIRED   CURRENT   READY   AGE
replicaset.apps/minio-d9848974d    1         1         1       23m
replicaset.apps/velero-b777b58c8   1         1         1       14m

NAME                    COMPLETIONS   DURATION   AGE
job.batch/minio-setup   1/1           30s        23m
```

Now, create the ConfigMap for the restore helper - during a restore of Pods with Persistent Volumes that have been backed up with restic, a temporary pod is instantiated to assist with the restore.

```yaml
cat << EOF > restic-config-map.yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: restic-restore-action-config
  namespace: velero
  labels:
    velero.io/plugin-config: ""
    velero.io/restic: RestoreItemAction
data:
  image: ${REGISTRY_HOST}/${REGISTRY_PROJECT}/velero-restic-restore-helper:v1.1.0
EOF
```

Deploy it

```console
ubuntu@lab1-ubu:~/velero-v1.1.0-linux-amd64$ kubectl apply -f restic-config-map.yaml
configmap/restic-restore-action-config created
```

## Test backup - base (no PV(C))

### Prepare the base.yaml

Edit the file `examples/nginx-app/base.yaml`

```shell
vim examples/nginx-app/base.yaml
```

replace the image nginx to the local repo `image: harbor.corp.local/public-repo/nginx:latest`

Automated -  
In a one-liner:

```shell
sed -i "s@image: nginx:.*@image: ${REGISTRY_HOST}/${REGISTRY_PROJECT}/nginx:latest@" examples/nginx-app/base.yaml
```

### Apply base and check

```console
ubuntu@lab1-ubu:~/velero-v1.1.0-linux-amd64$ kubectl apply -f examples/nginx-app/base.yaml
namespace/nginx-example created
deployment.apps/nginx-deployment created
service/my-nginx created
ubuntu@lab1-ubu:~/velero-v1.1.0-linux-amd64$ kubectl get all -n nginx-example
NAME                                   READY   STATUS    RESTARTS   AGE
pod/nginx-deployment-647896c67-kxwfk   1/1     Running   0          48s
pod/nginx-deployment-647896c67-p569x   1/1     Running   0          48s

NAME               TYPE           CLUSTER-IP       EXTERNAL-IP    PORT(S)        AGE
service/my-nginx   LoadBalancer   10.100.200.198   172.16.11.24   80:31423/TCP   49s

NAME                               READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/nginx-deployment   2/2     2            2           50s

NAME                                         DESIRED   CURRENT   READY   AGE
replicaset.apps/nginx-deployment-647896c67   2         2         2       49s
```

Open the url on brower (external ip), the nginx page shows up

### Backup base

Launch the backup

```console
ubuntu@lab1-ubu:~/velero-v1.1.0-linux-amd64$ velero backup create nginx-backup --selector app=nginx
Backup request "nginx-backup" submitted successfully.
Run `velero backup describe nginx-backup` or `velero backup logs nginx-backup` for more details.
```

see if it worked

```console
ubuntu@lab1-ubu:~/velero-v1.1.0-linux-amd64$ velero backup get
NAME           STATUS      CREATED                         EXPIRES   STORAGE LOCATION   SELECTOR
nginx-backup   Completed   2019-12-02 01:41:42 +0100 CET   29d       default            app=nginx
```

Browse on Minio url, see if there is some populated files in the `velero` bucket

### Delete base

Simulate a disaster by deleting everything of the base

```console
ubuntu@lab1-ubu:~/velero-v1.1.0-linux-amd64$ kubectl delete -f examples/nginx-app/base.yaml
namespace "nginx-example" deleted
deployment.apps "nginx-deployment" deleted
service "my-nginx" deleted

ubuntu@lab1-ubu:~/velero-v1.1.0-linux-amd64$ kubectl get ns
NAME              STATUS   AGE
default           Active   96m
kube-node-lease   Active   96m
kube-public       Active   96m
kube-system       Active   96m
pks-system        Active   64m
velero            Active   44m

ubuntu@lab1-ubu:~/velero-v1.1.0-linux-amd64$ kubectl get svc --all-namespaces
NAMESPACE     NAME                   TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)             AGE
default       kubernetes             ClusterIP   10.100.200.1     <none>        443/TCP             96m
kube-system   kube-dns               ClusterIP   10.100.200.2     <none>        53/UDP,53/TCP       64m
kube-system   kubernetes-dashboard   NodePort    10.100.200.156   <none>        443:31679/TCP       64m
kube-system   metrics-server         ClusterIP   10.100.200.143   <none>        443/TCP             64m
pks-system    fluent-bit             ClusterIP   10.100.200.110   <none>        24224/TCP           63m
pks-system    kube-state-metrics     ClusterIP   10.100.200.35    <none>        8080/TCP,8081/TCP   59m
pks-system    validator              ClusterIP   10.100.200.24    <none>        443/TCP             63m
pks-system    wavefront-proxy        ClusterIP   10.100.200.175   <none>        2878/TCP            59m
velero        minio                  NodePort    10.100.200.92    <none>        9000:32490/TCP      44m
```

### Restore base

```console
ubuntu@lab1-ubu:~/velero-v1.1.0-linux-amd64$ velero restore create nginx-restore --from-backup nginx-backup
Restore request "nginx-restore" submitted successfully.
Run `velero restore describe nginx-restore` or `velero restore logs nginx-restore` for more details.
ubuntu@lab1-ubu:~/velero-v1.1.0-linux-amd64$ velero restore describe nginx-restore
Name:         nginx-restore
Namespace:    velero
Labels:       <none>
Annotations:  <none>

Phase:  Completed

Backup:  nginx-backup

Namespaces:
  Included:  *
  Excluded:  <none>

Resources:
  Included:        *
  Excluded:        nodes, events, events.events.k8s.io, backups.velero.io, restores.velero.io, resticrepositories.velero.io
  Cluster-scoped:  auto

Namespace mappings:  <none>

Label selector:  <none>

Restore PVs:  auto
```

It completes with success. Let's check

```console
ubuntu@lab1-ubu:~/velero-v1.1.0-linux-amd64$ kubectl get ns
NAME              STATUS   AGE
default           Active   97m
kube-node-lease   Active   97m
kube-public       Active   97m
kube-system       Active   97m
nginx-example     Active   24s
pks-system        Active   65m
velero            Active   45m
ubuntu@lab1-ubu:~/velero-v1.1.0-linux-amd64$  kubectl get svc --all-namespaces
NAMESPACE       NAME                   TYPE           CLUSTER-IP       EXTERNAL-IP    PORT(S)             AGE
default         kubernetes             ClusterIP      10.100.200.1     <none>         443/TCP             98m
kube-system     kube-dns               ClusterIP      10.100.200.2     <none>         53/UDP,53/TCP       66m
kube-system     kubernetes-dashboard   NodePort       10.100.200.156   <none>         443:31679/TCP       65m
kube-system     metrics-server         ClusterIP      10.100.200.143   <none>         443/TCP             65m
nginx-example   my-nginx               LoadBalancer   10.100.200.163   172.16.11.26   80:30119/TCP        28s
pks-system      fluent-bit             ClusterIP      10.100.200.110   <none>         24224/TCP           65m
pks-system      kube-state-metrics     ClusterIP      10.100.200.35    <none>         8080/TCP,8081/TCP   60m
pks-system      validator              ClusterIP      10.100.200.24    <none>         443/TCP             65m
pks-system      wavefront-proxy        ClusterIP      10.100.200.175   <none>         2878/TCP            60m
velero          minio                  NodePort       10.100.200.92    <none>         9000:32490/TCP      45m
```

Note: _The IP address will change_

### Clean base

```console
ubuntu@lab1-ubu:~/velero-v1.1.0-linux-amd64$ kubectl delete -f examples/nginx-app/base.yaml
namespace "nginx-example" deleted
deployment.apps "nginx-deployment" deleted
service "my-nginx" deleted
```

## Test backup - add a PV(C)

:warning: Important! The "with-pv.yaml" uses snapshot features, which are not available in all in-tree and not in CSI before 1.16.

### Prepare for SC, PVC, PV

In vSphere, select the **Datastore**, expand **Tags**, clic on **Assign**.  
Create the tag with, for example, category **data** and value **bronze**.  
Create a **VM Storage Policy**, which is matching the new tag freshly created. Compatible Datastores must not be empty.

build a vm storage policy

```yaml
cat << EOF > sc-vcp.yaml
apiVersion: storage.k8s.io/v1
kind: StorageClass
metadata:
  name: sc-vcp
provisioner: kubernetes.io/vsphere-volume
parameters:
  storagePolicyName: bronze
EOF
```

Create the SC

```shell
kubectl apply -f sc-vcp.yaml
```

Test by creating a PV

```shell
cat << EOF > pvc-test.yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: pvc-test
spec:
  storageClassName: sc-vcp
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 1Gi
EOF
```

Apply and view

```console
ubuntu@lab1-ubu:~/velero-v1.1.0-linux-amd64$ kubectl apply -f pvc-test.yaml
persistentvolumeclaim/pvc-test created
ubuntu@lab1-ubu:~/velero-v1.1.0-linux-amd64$ kubectl get pvc
NAME       STATUS   VOLUME                                     CAPACITY   ACCESS MODES   STORAGECLASS   AGE
pvc-test   Bound    pvc-42ffa5a3-1e93-4556-8b49-b88cffcb889e   1Gi        RWO            sc-vcp         5s
```

Clean

```console
kubectl delete pvc pvc-test
```

### Prepare the base-pv.yaml

edit the lines to match your env. StorageClassName to match the create sc, ...

```yaml
cat << EOF > examples/nginx-app/base-pv.yaml
---
apiVersion: v1
kind: Namespace
metadata:
  name: nginx-example
  labels:
    app: nginx

---
kind: PersistentVolumeClaim
apiVersion: v1
metadata:
  name: nginx-logs
  namespace: nginx-example
  labels:
    app: nginx
spec:
  storageClassName: sc-vcp
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 50Mi

---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-deployment
  namespace: nginx-example
spec:
  replicas: 1
  selector:
    matchLabels:
      app: nginx
  template:
    metadata:
      labels:
        app: nginx
    spec:
      volumes:
        - name: nginx-logs
          persistentVolumeClaim:
           claimName: nginx-logs
      containers:
      - image: ${REGISTRY_HOST}/${REGISTRY_PROJECT}/nginx
        name: nginx
        ports:
        - containerPort: 80
        volumeMounts:
          - mountPath: "/var/log/nginx"
            name: nginx-logs
            readOnly: false

---
apiVersion: v1
kind: Service
metadata:
  labels:
    app: nginx
  name: my-nginx
  namespace: nginx-example
spec:
  ports:
  - port: 80
    targetPort: 80
  selector:
    app: nginx
  type: LoadBalancer
EOF
```

Apply the model

```console
ubuntu@lab1-ubu:~/velero-v1.1.0-linux-amd64$ kubectl apply -f examples/nginx-app/base-pv.yaml
namespace/nginx-example created
persistentvolumeclaim/nginx-logs created
deployment.apps/nginx-deployment created
service/my-nginx created
```

### Backup base-pv

Get the pod name, pv, pvc mappings

```console
ubuntu@lab1-ubu:~/velero-v1.1.0-linux-amd64$ kubectl -n nginx-example get pod,svc,pv,pvc -o wide
NAME                                    READY   STATUS    RESTARTS   AGE   IP          NODE                                   NOMINATED NODE   READINESS GATES
pod/nginx-deployment-64855b85b4-6l7nc   1/1     Running   0          14h   5.191.6.2   cfa9583e-fa66-4f7e-be58-ee967e1fb549   <none>           <none>

NAME               TYPE           CLUSTER-IP      EXTERNAL-IP    PORT(S)        AGE   SELECTOR
service/my-nginx   LoadBalancer   10.100.200.97   172.16.11.19   80:30347/TCP   15h   app=nginx

NAME                                                        CAPACITY   ACCESS MODES   RECLAIM POLICY   STATUS   CLAIM                      STORAGECLASS   REASON   AGE   VOLUMEMODE
persistentvolume/pvc-ccbdd2b3-8953-486b-a2e2-93ef648fa10c   50Mi       RWO            Delete           Bound    nginx-example/nginx-logs   sc-vcp                  14h   Filesystem

NAME                               STATUS   VOLUME                                     CAPACITY   ACCESS MODES   STORAGECLASS   AGE   VOLUMEMODE
persistentvolumeclaim/nginx-logs   Bound    pvc-ccbdd2b3-8953-486b-a2e2-93ef648fa10c   50Mi       RWO            sc-vcp         14h   Filesystem

ubuntu@lab1-ubu:~/velero-v1.1.0-linux-amd64$ kubectl -n nginx-example describe persistentvolume/pvc-ccbdd2b3-8953-486b-a2e2-93ef648fa10c
Name:            pvc-ccbdd2b3-8953-486b-a2e2-93ef648fa10c
Labels:          <none>
Annotations:     app: nginx
                 kubernetes.io/createdby: vsphere-volume-dynamic-provisioner
                 pv.kubernetes.io/bound-by-controller: yes
                 pv.kubernetes.io/provisioned-by: kubernetes.io/vsphere-volume
Finalizers:      [kubernetes.io/pv-protection]
StorageClass:    sc-vcp
Status:          Bound
Claim:           nginx-example/nginx-logs
Reclaim Policy:  Delete
Access Modes:    RWO
VolumeMode:      Filesystem
Capacity:        50Mi
Node Affinity:   <none>
Message:
Source:
    Type:               vSphereVolume (a Persistent Disk resource in vSphere)
    VolumePath:         [NFS] kubevols/kubernetes-dynamic-pvc-ccbdd2b3-8953-486b-a2e2-93ef648fa10c.vmdk
    FSType:             ext4
    StoragePolicyName:  bronze
Events:                 <none>

ubuntu@lab1-ubu:~/velero-v1.1.0-linux-amd64$ kubectl -n nginx-example describe pod/nginx-deployment-64855b85b4-6l7nc
Name:           nginx-deployment-64855b85b4-6l7nc
Namespace:      nginx-example
Priority:       0
Node:           cfa9583e-fa66-4f7e-be58-ee967e1fb549/5.197.0.3
Start Time:     Wed, 04 Dec 2019 00:46:32 +0100
Labels:         app=nginx
                pod-template-hash=64855b85b4
Annotations:    <none>
Status:         Running
IP:             5.191.6.2
IPs:            <none>
Controlled By:  ReplicaSet/nginx-deployment-64855b85b4
Containers:
  nginx:
    Container ID:   docker://18e7d3c882d80014151bad59e26f8fc72ee976c03ebfc991743c081b23168f68
    Image:          harbor.corp.local/public-repo/nginx
    Image ID:       docker-pullable://harbor.corp.local/public-repo/nginx@sha256:189cce606b29fb2a33ebc2fcecfa8e33b0b99740da4737133cdbcee92f3aba0a
    Port:           80/TCP
    Host Port:      0/TCP
    State:          Running
      Started:      Wed, 04 Dec 2019 00:46:42 +0100
    Ready:          True
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /var/log/nginx from nginx-logs (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-fd6hj (ro)
Conditions:
  Type              Status
  Initialized       True
  Ready             True
  ContainersReady   True
  PodScheduled      True
Volumes:
  nginx-logs:
    Type:       PersistentVolumeClaim (a reference to a PersistentVolumeClaim in the same namespace)
    ClaimName:  nginx-logs
    ReadOnly:   false
  default-token-fd6hj:
    Type:        Secret (a volume populated by a Secret)
    SecretName:  default-token-fd6hj
    Optional:    false
QoS Class:       BestEffort
Node-Selectors:  <none>
Tolerations:     node.kubernetes.io/not-ready:NoExecute for 300s
                 node.kubernetes.io/unreachable:NoExecute for 300s
Events:          <none>
```

Add the annotation to pod for the pv to backup.  
"backup-volumes" is the name of the volume, here is nginx-logs  
"pod/[...]" is the pod where volume is attached

```console
ubuntu@lab1-ubu:~/velero-v1.1.0-linux-amd64$ kubectl -n nginx-example annotate pod/nginx-deployment-64855b85b4-6l7nc backup.velero.io/backup-volumes=nginx-logs
pod/nginx-deployment-64855b85b4-6l7nc annotated
```

Then create the backup

```console
ubuntu@lab1-ubu:~/velero-v1.1.0-linux-amd64$ velero backup create nginx-backup-pv --include-namespaces nginx-example
Backup request "nginx-backup-pv" submitted successfully.
Run `velero backup describe nginx-backup-pv` or `velero backup logs nginx-backup-pv` for more details.
```

Check status:

```console
ubuntu@lab1-ubu:~/velero-v1.1.0-linux-amd64$ velero backup get
NAME              STATUS       CREATED                         EXPIRES   STORAGE LOCATION   SELECTOR
nginx-backup-pv   InProgress   2019-12-04 15:51:29 +0100 CET   29d       default            <none>
ubuntu@lab1-ubu:~/velero-v1.1.0-linux-amd64$ velero backup get
NAME              STATUS      CREATED                         EXPIRES   STORAGE LOCATION   SELECTOR
nginx-backup-pv   Completed   2019-12-04 15:51:29 +0100 CET   29d       default            <none>
```

Browse on Minio url, see if there is some populated files in the `velero` bucket  
In the minio UI, there should be the folder for restic too now.

Check the description

```console
ubuntu@lab1-ubu:~/velero-v1.1.0-linux-amd64$ velero backup describe nginx-backup-pv --details
Name:         nginx-backup-pv
Namespace:    velero
Labels:       velero.io/storage-location=default
Annotations:  <none>

Phase:  Completed

Namespaces:
  Included:  nginx-example
  Excluded:  <none>

Resources:
  Included:        *
  Excluded:        <none>
  Cluster-scoped:  auto

Label selector:  <none>

Storage Location:  default

Snapshot PVs:  auto

TTL:  720h0m0s

Hooks:  <none>

Backup Format Version:  1

Started:    2019-12-04 15:51:29 +0100 CET
Completed:  2019-12-04 15:51:40 +0100 CET

Expiration:  2020-01-03 15:51:29 +0100 CET

Resource List:
  apps/v1/Deployment:
    - nginx-example/nginx-deployment
  apps/v1/ReplicaSet:
    - nginx-example/nginx-deployment-64855b85b4
  v1/Endpoints:
    - nginx-example/my-nginx
  v1/Namespace:
    - nginx-example
  v1/PersistentVolume:
    - pvc-ccbdd2b3-8953-486b-a2e2-93ef648fa10c
  v1/PersistentVolumeClaim:
    - nginx-example/nginx-logs
  v1/Pod:
    - nginx-example/nginx-deployment-64855b85b4-6l7nc
  v1/Secret:
    - nginx-example/default-token-fd6hj
  v1/Service:
    - nginx-example/my-nginx
  v1/ServiceAccount:
    - nginx-example/default

Persistent Volumes: <none included>

Restic Backups:
  Completed:
    nginx-example/nginx-deployment-64855b85b4-6l7nc: nginx-logs
```

And the CRD

```console
ubuntu@lab1-ubu:~/velero-v1.1.0-linux-amd64$ kubectl -n velero get podvolumebackups -l velero.io/backup-name=nginx-backup-pv -o yaml
apiVersion: v1
items:
- apiVersion: velero.io/v1
  kind: PodVolumeBackup
  metadata:
    annotations:
      velero.io/pvc-name: nginx-logs
    creationTimestamp: "2019-12-04T14:51:34Z"
    generateName: nginx-backup-pv-
    generation: 3
    labels:
      velero.io/backup-name: nginx-backup-pv
      velero.io/backup-uid: b8f0df22-3a36-4d46-881e-7f6b3c11c2ae
    name: nginx-backup-pv-gg5qc
    namespace: velero
    ownerReferences:
    - apiVersion: velero.io/v1
      controller: true
      kind: Backup
      name: nginx-backup-pv
      uid: b8f0df22-3a36-4d46-881e-7f6b3c11c2ae
    resourceVersion: "149429"
    selfLink: /apis/velero.io/v1/namespaces/velero/podvolumebackups/nginx-backup-pv-gg5qc
    uid: 02062b6e-e515-42fb-b957-ddbf6e287e46
  spec:
    backupStorageLocation: default
    node: cfa9583e-fa66-4f7e-be58-ee967e1fb549
    pod:
      kind: Pod
      name: nginx-deployment-64855b85b4-6l7nc
      namespace: nginx-example
      uid: a5444c4f-e24e-4a1a-a115-e4ff7e45f1d3
    repoIdentifier: s3:http://minio.velero.svc:9000/velero/restic/nginx-example
    tags:
      backup: nginx-backup-pv
      backup-uid: b8f0df22-3a36-4d46-881e-7f6b3c11c2ae
      ns: nginx-example
      pod: nginx-deployment-64855b85b4-6l7nc
      pod-uid: a5444c4f-e24e-4a1a-a115-e4ff7e45f1d3
      volume: nginx-logs
    volume: nginx-logs
  status:
    completionTimestamp: "2019-12-04T14:51:39Z"
    message: ""
    path: /host_pods/a5444c4f-e24e-4a1a-a115-e4ff7e45f1d3/volumes/kubernetes.io~vsphere-volume/pvc-ccbdd2b3-8953-486b-a2e2-93ef648fa10c
    phase: Completed
    snapshotID: 3b2116a1
    startTimestamp: "2019-12-04T14:51:34Z"
kind: List
metadata:
  resourceVersion: ""
  selfLink: ""
```

### Delete base-pv

Simulate a disaster by deleting everything of the base-pv

```console
ubuntu@lab1-ubu:~/velero-v1.1.0-linux-amd64$ kubectl delete -f examples/nginx-app/base-pv.yaml
namespace "nginx-example" deleted
persistentvolumeclaim "nginx-logs" deleted
deployment.apps "nginx-deployment" deleted
service "my-nginx" deleted
```

See if there is any remaining resources

```
ubuntu@lab1-ubu:~/velero-v1.1.0-linux-amd64$ kubectl get pv,pvc -A
No resources found
ubuntu@lab1-ubu:~/velero-v1.1.0-linux-amd64$ kubectl get ns
NAME              STATUS   AGE
default           Active   24h
kube-node-lease   Active   24h
kube-public       Active   24h
kube-system       Active   24h
pks-system        Active   24h
velero            Active   23h
```

No PV, no Pods, no Namespace.

### Restore base-pv

```console
ubuntu@lab1-ubu:~/velero-v1.1.0-linux-amd64$ velero restore create nginx-restore --from-backup nginx-backup-pv
Restore request "nginx-restore" submitted successfully.
Run `velero restore describe nginx-restore` or `velero restore logs nginx-restore` for more details.
```

Details on the restore

```console
ubuntu@lab1-ubu:~/velero-v1.1.0-linux-amd64$ velero restore describe nginx-restore | grep Phase
Phase:  InProgress
ubuntu@lab1-ubu:~/velero-v1.1.0-linux-amd64$ velero restore describe nginx-restore --details
Name:         nginx-restore
Namespace:    velero
Labels:       <none>
Annotations:  <none>

Phase:  Completed

Backup:  nginx-backup-pv

Namespaces:
  Included:  *
  Excluded:  <none>

Resources:
  Included:        *
  Excluded:        nodes, events, events.events.k8s.io, backups.velero.io, restores.velero.io, resticrepositories.velero.io
  Cluster-scoped:  auto

Namespace mappings:  <none>

Label selector:  <none>

Restore PVs:  auto

Restic Restores:
  Completed:
    nginx-example/nginx-deployment-64855b85b4-6l7nc: nginx-logs
```

It completes with success. Let's check

```console
ubuntu@lab1-ubu:~/velero-v1.1.0-linux-amd64$ kubectl get ns
NAME              STATUS   AGE
default           Active   24h
kube-node-lease   Active   24h
kube-public       Active   24h
kube-system       Active   24h
nginx-example     Active   118s
pks-system        Active   24h
velero            Active   24h
ubuntu@lab1-ubu:~/velero-v1.1.0-linux-amd64$  kubectl -n nginx-example get pods,svc,pv,pvc
NAME                                    READY   STATUS    RESTARTS   AGE
pod/nginx-deployment-64855b85b4-6l7nc   1/1     Running   0          2m42s

NAME               TYPE           CLUSTER-IP     EXTERNAL-IP    PORT(S)        AGE
service/my-nginx   LoadBalancer   10.100.200.9   172.16.11.19   80:31984/TCP   2m42s

NAME                                                        CAPACITY   ACCESS MODES   RECLAIM POLICY   STATUS   CLAIM                      STORAGECLASS   REASON   AGE
persistentvolume/pvc-11e40aed-7bc5-492c-990f-c7c43aeddb51   50Mi       RWO            Delete           Bound    nginx-example/nginx-logs   sc-vcp                  2m40s

NAME                               STATUS   VOLUME                                     CAPACITY   ACCESS MODES   STORAGECLASS   AGE
persistentvolumeclaim/nginx-logs   Bound    pvc-11e40aed-7bc5-492c-990f-c7c43aeddb51   50Mi       RWO            sc-vcp         2m43s
```

Note: _The IP address may/will change_

### Clean base-pv

```console
ubuntu@lab1-ubu:~/velero-v1.1.0-linux-amd64$ velero backup delete nginx-backup-pv --confirm
Request to delete backup "nginx-backup-pv" submitted successfully.
The backup will be fully deleted after all associated data (disk snapshots, backup files, restores) are removed.
```

## Uninstall Velero

```shell
kubectl delete namespace/velero clusterrolebind