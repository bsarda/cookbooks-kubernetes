# Installing knative

Prereq: Helm must be installed first.
Prereq: Istio must be installed first.

## Install

```shell
kubectl apply --selector knative.dev/crd-install=true \
--filename https://github.com/knative/serving/releases/download/v0.8.0/serving.yaml \
--filename https://github.com/knative/eventing/releases/download/v0.8.0/release.yaml \
--filename https://github.com/knative/serving/releases/download/v0.8.0/monitoring.yaml


		customresourcedefinition.apiextensions.k8s.io/certificates.networking.internal.knative.dev created
		customresourcedefinition.apiextensions.k8s.io/clusteringresses.networking.internal.knative.dev created
		customresourcedefinition.apiextensions.k8s.io/images.caching.internal.knative.dev created
		customresourcedefinition.apiextensions.k8s.io/ingresses.networking.internal.knative.dev created
		customresourcedefinition.apiextensions.k8s.io/metrics.autoscaling.internal.knative.dev created
		customresourcedefinition.apiextensions.k8s.io/podautoscalers.autoscaling.internal.knative.dev created
		customresourcedefinition.apiextensions.k8s.io/serverlessservices.networking.internal.knative.dev created
		customresourcedefinition.apiextensions.k8s.io/configurations.serving.knative.dev created
		customresourcedefinition.apiextensions.k8s.io/revisions.serving.knative.dev created
		customresourcedefinition.apiextensions.k8s.io/routes.serving.knative.dev created
		customresourcedefinition.apiextensions.k8s.io/services.serving.knative.dev created
		customresourcedefinition.apiextensions.k8s.io/apiserversources.sources.eventing.knative.dev created
		customresourcedefinition.apiextensions.k8s.io/brokers.eventing.knative.dev created
		customresourcedefinition.apiextensions.k8s.io/channels.eventing.knative.dev created
		customresourcedefinition.apiextensions.k8s.io/channels.messaging.knative.dev created
		customresourcedefinition.apiextensions.k8s.io/choices.messaging.knative.dev created
		customresourcedefinition.apiextensions.k8s.io/clusterchannelprovisioners.eventing.knative.dev created
		customresourcedefinition.apiextensions.k8s.io/containersources.sources.eventing.knative.dev created
		customresourcedefinition.apiextensions.k8s.io/cronjobsources.sources.eventing.knative.dev created
		customresourcedefinition.apiextensions.k8s.io/eventtypes.eventing.knative.dev created
		customresourcedefinition.apiextensions.k8s.io/sequences.messaging.knative.dev created
		customresourcedefinition.apiextensions.k8s.io/subscriptions.eventing.knative.dev created
		customresourcedefinition.apiextensions.k8s.io/triggers.eventing.knative.dev created
		customresourcedefinition.apiextensions.k8s.io/inmemorychannels.messaging.knative.dev created
```

```shell
kubectl apply --filename https://github.com/knative/serving/releases/download/v0.8.0/serving.yaml \
--filename https://github.com/knative/eventing/releases/download/v0.8.0/release.yaml \
--filename https://github.com/knative/serving/releases/download/v0.8.0/monitoring.yaml
```

## Wait until completion

monitor until everything is running

```shell
kubectl get pods -n knative-serving
kubectl get pods -n knative-eventing
kubectl get pods -n knative-monitoring
```

## Test with helloworld in go


