# Installing Istio

Prereq: Helm must be installed first.

## Download

Download istio

`export ISTIO_VERSION=1.2.5`

```shell
curl -L https://git.io/getLatestIstio | sh -


      % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                    Dload  Upload   Total   Spent    Left  Speed
      0     0    0     0    0     0      0      0 --:--:-- --:--:-- --:--:--     0
    100  2207  100  2207    0     0   5242      0 --:--:-- --:--:-- --:--:--  5242
    Downloading istio-1.2.5 from https://github.com/istio/istio/releases/download/1.2.5/istio-1.2.5-linux.tar.gz ...  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                    Dload  Upload   Total   Spent    Left  Speed
    100   614    0   614    0     0   2716      0 --:--:-- --:--:-- --:--:--  2704
    100 30.9M  100 30.9M    0     0  11.6M      0  0:00:02  0:00:02 --:--:-- 19.5M
    Istio 1.2.5 Download Complete!

    Istio has been successfully downloaded into the istio-1.2.5 folder on your system.

    Next Steps:
    See https://istio.io/docs/setup/kubernetes/install/ to add Istio to your Kubernetes cluster.

    To configure the istioctl client tool for your workstation,
    add the /home/ubuntu/istio-1.2.5/bin directory to your environment path variable with:
            export PATH="$PATH:/home/ubuntu/istio-1.2.5/bin"

    Begin the Istio pre-installation verification check by running:
            istioctl verify-install

    Need more information? Visit https://istio.io/docs/setup/kubernetes/install/
```

## Install istioctl and precheck

Move the binary

```shell
cd istio-${ISTIO_VERSION}
sudo mv bin/istioctl /usr/local/bin/istioctl
cd ..
```

Do the precheck

```shell
istioctl verify-install
```

## Install the CRDs

```shell
cd istio-${ISTIO_VERSION}
for i in install/kubernetes/helm/istio-init/files/crd*yaml; do kubectl apply -f $i; done


    customresourcedefinition.apiextensions.k8s.io/virtualservices.networking.istio.io created
    customresourcedefinition.apiextensions.k8s.io/destinationrules.networking.istio.io created
    customresourcedefinition.apiextensions.k8s.io/serviceentries.networking.istio.io created
    customresourcedefinition.apiextensions.k8s.io/gateways.networking.istio.io created
    customresourcedefinition.apiextensions.k8s.io/envoyfilters.networking.istio.io created
    customresourcedefinition.apiextensions.k8s.io/clusterrbacconfigs.rbac.istio.io created
    customresourcedefinition.apiextensions.k8s.io/policies.authentication.istio.io created
    customresourcedefinition.apiextensions.k8s.io/meshpolicies.authentication.istio.io created
    customresourcedefinition.apiextensions.k8s.io/httpapispecbindings.config.istio.io created
    customresourcedefinition.apiextensions.k8s.io/httpapispecs.config.istio.io created
    customresourcedefinition.apiextensions.k8s.io/quotaspecbindings.config.istio.io created
    customresourcedefinition.apiextensions.k8s.io/quotaspecs.config.istio.io created
    customresourcedefinition.apiextensions.k8s.io/rules.config.istio.io created
    customresourcedefinition.apiextensions.k8s.io/attributemanifests.config.istio.io created
    customresourcedefinition.apiextensions.k8s.io/rbacconfigs.rbac.istio.io created
    customresourcedefinition.apiextensions.k8s.io/serviceroles.rbac.istio.io created
    customresourcedefinition.apiextensions.k8s.io/servicerolebindings.rbac.istio.io created
    customresourcedefinition.apiextensions.k8s.io/adapters.config.istio.io created
    customresourcedefinition.apiextensions.k8s.io/instances.config.istio.io created
    customresourcedefinition.apiextensions.k8s.io/templates.config.istio.io created
    customresourcedefinition.apiextensions.k8s.io/handlers.config.istio.io created
    customresourcedefinition.apiextensions.k8s.io/sidecars.networking.istio.io created
    customresourcedefinition.apiextensions.k8s.io/authorizationpolicies.rbac.istio.io created
    customresourcedefinition.apiextensions.k8s.io/clusterissuers.certmanager.k8s.io created
    customresourcedefinition.apiextensions.k8s.io/issuers.certmanager.k8s.io created
    customresourcedefinition.apiextensions.k8s.io/certificates.certmanager.k8s.io created
    customresourcedefinition.apiextensions.k8s.io/orders.certmanager.k8s.io created
    customresourcedefinition.apiextensions.k8s.io/challenges.certmanager.k8s.io created
```

wait a few seconds, then create the namespace

```shell
cat <<-EOF | kubectl apply -f -
   apiVersion: v1
   kind: Namespace
   metadata:
     name: istio-system
     labels:
       istio-injection: disabled
EOF

    namespace/istio-system created
```

## Check if sidecar can be automatically injected

Check that the admission plugins are enabled, to allow automatic injection of sidecar.  

On PKS:  
Connect to one of the master nodes, get the cluster UUID from cluster name: `CLUSTER_NAME=kn2`

```shell
CLUSTERID=$(pks clusters | grep "${CLUSTER_NAME}" | awk '{printf $3}')
bosh -d service-instance_${CLUSTERID} ssh master/0
```

Then validate the admission plugins are found (the lines should return a line)

```shell
sudo -i
/var/vcap/packages/kubernetes/bin/kube-apiserver -h | grep enable-admission-plugins | grep MutatingAdmissionWebhook
/var/vcap/packages/kubernetes/bin/kube-apiserver -h | grep enable-admission-plugins | grep ValidatingAdmissionWebhook
```

## Deploy Istio with sidecar injection enabled

Create the Helm template with sidecar injection enabled.

```shell
helm template --namespace=istio-system \
  --set sidecarInjectorWebhook.enabled=true \
  --set sidecarInjectorWebhook.enableNamespacesByDefault=true \
  --set global.proxy.autoInject=disabled \
  --set global.disablePolicyChecks=true \
  --set prometheus.enabled=false \
  `# Disable mixer prometheus adapter to remove istio default metrics.` \
  --set mixer.adapters.prometheus.enabled=false \
  `# Disable mixer policy check, since in our template we set no policy.` \
  --set global.disablePolicyChecks=true \
  `# Set gateway pods to 1 to sidestep eventual consistency / readiness problems.` \
  --set gateways.istio-ingressgateway.autoscaleMin=1 \
  --set gateways.istio-ingressgateway.autoscaleMax=1 \
  --set gateways.istio-ingressgateway.resources.requests.cpu=500m \
  --set gateways.istio-ingressgateway.resources.requests.memory=256Mi \
  `# More pilot replicas for better scale` \
  --set pilot.autoscaleMin=2 \
  `# Set pilot trace sampling to 100%` \
  --set pilot.traceSampling=100 \
  install/kubernetes/helm/istio \
> ./istio.yaml
```

This creates the k8s yaml for you. then, just apply !

```shell
kubectl apply -f istio.yaml


    poddisruptionbudget.policy/istio-galley created
    poddisruptionbudget.policy/istio-ingressgateway created
    poddisruptionbudget.policy/istio-policy created
    poddisruptionbudget.policy/istio-telemetry created
    poddisruptionbudget.policy/istio-pilot created
    poddisruptionbudget.policy/istio-sidecar-injector created
    configmap/istio-galley-configuration created
    configmap/istio-security-custom-resources created
    configmap/istio created
    configmap/istio-sidecar-injector created
    serviceaccount/istio-galley-service-account created
    serviceaccount/istio-ingressgateway-service-account created
    serviceaccount/istio-mixer-service-account created
    serviceaccount/istio-pilot-service-account created
    serviceaccount/istio-cleanup-secrets-service-account created
    clusterrole.rbac.authorization.k8s.io/istio-cleanup-secrets-istio-system created
    clusterrolebinding.rbac.authorization.k8s.io/istio-cleanup-secrets-istio-system created
    job.batch/istio-cleanup-secrets-1.2.5 created
    serviceaccount/istio-security-post-install-account created
    clusterrole.rbac.authorization.k8s.io/istio-security-post-install-istio-system created
    clusterrolebinding.rbac.authorization.k8s.io/istio-security-post-install-role-binding-istio-system created
    job.batch/istio-security-post-install-1.2.5 created
    serviceaccount/istio-citadel-service-account created
    serviceaccount/istio-sidecar-injector-service-account created
    serviceaccount/istio-multi created
    clusterrole.rbac.authorization.k8s.io/istio-galley-istio-system created
    clusterrole.rbac.authorization.k8s.io/istio-mixer-istio-system created
    clusterrole.rbac.authorization.k8s.io/istio-pilot-istio-system created
    clusterrole.rbac.authorization.k8s.io/istio-citadel-istio-system created
    clusterrole.rbac.authorization.k8s.io/istio-sidecar-injector-istio-system created
    clusterrole.rbac.authorization.k8s.io/istio-reader created
    clusterrolebinding.rbac.authorization.k8s.io/istio-galley-admin-role-binding-istio-system created
    clusterrolebinding.rbac.authorization.k8s.io/istio-mixer-admin-role-binding-istio-system created
    clusterrolebinding.rbac.authorization.k8s.io/istio-pilot-istio-system created
    clusterrolebinding.rbac.authorization.k8s.io/istio-citadel-istio-system created
    clusterrolebinding.rbac.authorization.k8s.io/istio-sidecar-injector-admin-role-binding-istio-system created
    clusterrolebinding.rbac.authorization.k8s.io/istio-multi created
    role.rbac.authorization.k8s.io/istio-ingressgateway-sds created
    rolebinding.rbac.authorization.k8s.io/istio-ingressgateway-sds created
    service/istio-galley created
    service/istio-ingressgateway created
    service/istio-policy created
    service/istio-telemetry created
    service/istio-pilot created
    service/istio-citadel created
    service/istio-sidecar-injector created
    deployment.apps/istio-galley created
    deployment.apps/istio-ingressgateway created
    deployment.apps/istio-policy created
    deployment.apps/istio-telemetry created
    deployment.apps/istio-pilot created
    deployment.apps/istio-citadel created
    deployment.apps/istio-sidecar-injector created
    horizontalpodautoscaler.autoscaling/istio-ingressgateway created
    horizontalpodautoscaler.autoscaling/istio-policy created
    horizontalpodautoscaler.autoscaling/istio-telemetry created
    horizontalpodautoscaler.autoscaling/istio-pilot created
    mutatingwebhookconfiguration.admissionregistration.k8s.io/istio-sidecar-injector created
    attributemanifest.config.istio.io/istioproxy created
    attributemanifest.config.istio.io/kubernetes created
    handler.config.istio.io/kubernetesenv created
    rule.config.istio.io/kubeattrgenrulerule created
    rule.config.istio.io/tcpkubeattrgenrulerule created
    instance.config.istio.io/attributes created
    destinationrule.networking.istio.io/istio-policy created
    destinationrule.networking.istio.io/istio-telemetry created
```

wait until everything is ok

```shell
watch kubectl get all -n istio-system


    NAME                                          READY   STATUS      RESTARTS   AGE
    pod/istio-citadel-554dd94849-8nvv8            1/1     Running     0          6m29s
    pod/istio-cleanup-secrets-1.2.5-fthlw         0/1     Completed   0          6m32s
    pod/istio-galley-6fb4f7fb5d-vnzhc             1/1     Running     0          6m30s
    pod/istio-ingressgateway-8d84b7967-qf2wp      1/1     Running     0          6m30s
    pod/istio-pilot-787dcc796-dh54v               2/2     Running     0          6m13s
    pod/istio-pilot-787dcc796-hcv87               2/2     Running     0          6m30s
    pod/istio-policy-5fbb58c8ff-zj9ws             2/2     Running     0          6m30s
    pod/istio-security-post-install-1.2.5-brdpk   0/1     Completed   0          6m31s
    pod/istio-sidecar-injector-7bd8854fd9-fwn8s   1/1     Running     0          6m29s
    pod/istio-telemetry-6649978665-4ddvl          2/2     Running     3          6m30s

    NAME                             TYPE           CLUSTER-IP       EXTERNAL-IP                    PORT(S)                                                                                                                                      AGE
    service/istio-citadel            ClusterIP      10.100.200.16    <none>                         8060/TCP,15014/TCP                                                                                                                           6m31s
    service/istio-galley             ClusterIP      10.100.200.237   <none>                         443/TCP,15014/TCP,9901/TCP                                                                                                                   6m31s
    service/istio-ingressgateway     LoadBalancer   10.100.200.34    100.64.208.37,172.120.255.38   15020:30628/TCP,80:31380/TCP,443:31390/TCP,31400:31400/TCP,15029:30281/TCP,15030:31274/TCP,15031:31556/TCP,15032:30807/TCP,15443:30841/TCP   6m31s
    service/istio-pilot              ClusterIP      10.100.200.223   <none>                         15010/TCP,15011/TCP,8080/TCP,15014/TCP                                                                                                       6m31s
    service/istio-policy             ClusterIP      10.100.200.192   <none>                         9091/TCP,15004/TCP,15014/TCP                                                                                                                 6m31s
    service/istio-sidecar-injector   ClusterIP      10.100.200.158   <none>                         443/TCP                                                                                                                                      6m31s
    service/istio-telemetry          ClusterIP      10.100.200.224   <none>                         9091/TCP,15004/TCP,15014/TCP,42422/TCP                                                                                                       6m31s

    NAME                                     READY   UP-TO-DATE   AVAILABLE   AGE
    deployment.apps/istio-citadel            1/1     1            1           6m30s
    deployment.apps/istio-galley             1/1     1            1           6m31s
    deployment.apps/istio-ingressgateway     1/1     1            1           6m30s
    deployment.apps/istio-pilot              2/2     2            2           6m30s
    deployment.apps/istio-policy             1/1     1            1           6m30s
    deployment.apps/istio-sidecar-injector   1/1     1            1           6m29s
    deployment.apps/istio-telemetry          1/1     1            1           6m30s

    NAME                                                DESIRED   CURRENT   READY   AGE
    replicaset.apps/istio-citadel-554dd94849            1         1         1       6m29s
    replicaset.apps/istio-galley-6fb4f7fb5d             1         1         1       6m30s
    replicaset.apps/istio-ingressgateway-8d84b7967      1         1         1       6m30s
    replicaset.apps/istio-pilot-787dcc796               2         2         2       6m30s
    replicaset.apps/istio-policy-5fbb58c8ff             1         1         1       6m30s
    replicaset.apps/istio-sidecar-injector-7bd8854fd9   1         1         1       6m29s
    replicaset.apps/istio-telemetry-6649978665          1         1         1       6m30s

    NAME                                                       REFERENCE                         TARGETS   MINPODS   MAXPODS   REPLICAS   AGE
    horizontalpodautoscaler.autoscaling/istio-ingressgateway   Deployment/istio-ingressgateway   0%/80%    1         1         1          6m29s
    horizontalpodautoscaler.autoscaling/istio-pilot            Deployment/istio-pilot            3%/80%    2         5         2          6m29s
    horizontalpodautoscaler.autoscaling/istio-policy           Deployment/istio-policy           15%/80%   1         5         1          6m29s
    horizontalpodautoscaler.autoscaling/istio-telemetry        Deployment/istio-telemetry        1%/80%    1         5         1          6m29s

    NAME                                          COMPLETIONS   DURATION   AGE
    job.batch/istio-cleanup-secrets-1.2.5         1/1           2m32s      6m32s
    job.batch/istio-security-post-install-1.2.5   1/1           2m5s       6m31s
```

## Check

Proxy status

```shell
istioctl proxy-status


    NAME                                                  CDS        LDS        EDS               RDS          PILOT                           VERSION
    istio-ingressgateway-8d84b7967-qf2wp.istio-system     SYNCED     SYNCED     SYNCED (100%)     NOT SENT     istio-pilot-787dcc796-hcv87     1.2.5
```

And the versions

```shell
istioctl version


    client version: 1.2.5
    citadel version: 1.2.5
    galley version: 1.2.5
    ingressgateway version: 1.2.5
    pilot version: 1.2.5
    pilot version: 1.2.5
    policy version: 1.2.5
    sidecar-injector version: 1.2.5
    telemetry version: 1.2.5
```
