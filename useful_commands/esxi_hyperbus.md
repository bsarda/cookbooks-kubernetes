# Useful commands - ESXi hyperbus

## From the k8s worker nodes, get the status

```sh
sudo  /var/vcap/jobs/nsx-node-agent/bin/nsxcli -c get node-agent-hyperbus status
```

## From an ESXi,get the connection

if not communication_error

```sh
root@esxi # get hyperbus connection info
```

## Restart it on the ESXi

will restart the nsx stack

```sh
/etc/init.d/netcpad restart
```

