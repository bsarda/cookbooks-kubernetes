# Useful commands - UAA

## Set target and Login

The adminSecret is in Opsman, in the PKS tile, tab Credentials, item "Pks UAA Management Admin Client"

```sh
uaac target https://$PKSAPI_HOST:8443 --skip-ssl-validation
uaac token client get admin -s "$ADMIN_SECRET"
# Sample:
#uaac target https://pksapi.corp.local:8443 --skip-ssl-validation
#uaac token client get admin -s "EWQ4vbTHja0ZOC9_duKAdS8svotHh2Sx"
```

## Add LDAP group (or user) to roles

```sh
uaac group map --name pks.clusters.manage $GROUP_DN
uaac group map --name pks.clusters.admin $GROUP_DN
#Sample:
#uaac group map --name pks.clusters.manage CN=G_PKSAdmins,OU=DomainGroups,DC=corp,DC=local
#uaac group map --name pks.clusters.admin CN=G_PKSAdmins,OU=DomainGroups,DC=corp,DC=local
```

## Create a local user (if no LDAP)

```sh
uaac user add $USER_NAME --emails $USER_MAIL -p $USER_PASSWORD
#Sample:
#uaac user add bsarda --emails bsarda@corp.local -p VMware1!
```

## Add user to a roles

```sh
uaac member add pks.clusters.admin ${USER_NAME}
#Sample:
#uaac member add pks.clusters.admin bsarda
```

## Create a client with a secret, and add to roles

```sh
uaac client add $CLIENT_NAME -s $CLIENT_SECRET --authorized_grant_types client_credentials --authorities $UAA_SCOPES
#Sample:
#uaac client add automated-client -s randomly-generated-secret --authorized_grant_types client_credentials --authorities pks.clusters.admin,pks.clusters.manage
```

Source: [PKS Manage UAA Users](https://docs.pivotal.io/runtimes/pks/1-3/manage-users.html#cluster-access)
