# Useful commands - Kubernetes

## Get/list

### Get Cluster/nodes

Get the cluster info  
gives info where is running the master, the metrics/dns/dashboard/influx

```sh
kubectl cluster-info
```

View the config file of the cluster

```sh
kubectl config view
```

Get the nodes of the k8s cluster

```sh
kubectl get nodes -o wide
```

Explain how to create/understand an object (pod, rc, svc...)  
lists the metadata, apiversion, spec...

```sh
kubectl explain $OBJECT_TYPE
#Sample:
kubectl explain pod
```

Get a yaml of a deployment

```sh
kubectl -n $NAMESPACE get deploy $DEPLOYMENT_NAME -o yaml --export
#Sample:
kubectl -n kube-system get deploy kubernetes-dashboard -o yaml --export
```

### Get Objects

Get all the pods on all the namespaces

```sh
kubectl get pods -o wide --all-namespaces
```

Get pods on a given namespaces

```sh
kubectl -n $NAMESPACE_NAME get pods
#Sample:
kubectl -n demo1 get pods
```

Get all the deployments on all namespaces (note: ds is the abreviation for deployments)

```sh
kubectl get deployments -o wide --all-namespaces
```

Get deployments on a given namespaces

```sh
kubectl -n $NAMESPACE_NAME get ds
#Sample:
kubectl -n demo1 get ds
```

Get all the services on all namespaces (note: svc is the abreviation for services)

```sh
kubectl get svc -o wide --all-namespaces
```

Get services on a given namespaces

```sh
kubectl -n $NAMESPACE_NAME get svc
#Sample:
kubectl -n demo1 get svc
```

## Create/run

Create a namespace

```sh
kubectl create namespace $NAMESPACE_NAME
#Sample:
kubectl create namespace demo1
```

Create a pod from an image

```sh
kubectl run $POD_NAME --image=$IMAGE_NAME
#Sample:
kubectl run nginx-demo --image=nginx
```

Create _something_ from a manifest yaml

```sh
kubectl create -f $FILENAME
#Sample:
kubectl create -f /home/ubuntu/pks-deploy/9_create_clusters_and_test/k8s-yaml/demo1.yaml
```

Scale replica controller

```sh
kubectl -n $NAMESPACE_NAME scale --replicas=$COUNT rc $REPLICA_NAME
#Sample:
kubectl -n demo1 scale --replicas=4 rc nginx
```

Scale deployment's replicas

```sh
kubectl -n $NAMESPACE_NAME scale --replicas=$COUNT $RESOURCE_TYPE/$RESOURCE_NAME
#Sample:
kubectl -n demo1 scale --replicas=4 deployment/nginx
```

Expose an external port on a rc

```sh
kubectl -n $NAMESPACE_NAME expose rc $REPLICA_NAME --port=$EXTERNAL_PORT --target-port=$ESOURCE_TYPE/$RESOURCE_NAME
#Sample:
kubectl -n demo1 expose rc nginx --port 34853 --target-port=443
```

## Delete

Force delete a pod:

```sh
kubectl -n $NAMESPACE_NAME delete pods $POD_NAME --grace-period=0 --force
#Sample:
kubectl -n demo1 delete pods nginx --grace-period=0 --force
```

Drain a node from its pods:

```sh
kubectl drain $NODE_ID --grace-period 10 --force --delete-local-data --ignore-daemonsets
#Sample:
kubectl drain b061e7fa-d1d9-43e3-99f8-ef142c00c64f --grace-period 10 --force --delete-local-data --ignore-daemonsets
```

## Debug

Execute a command on the object (a pod)  
alternatively, you could specify the container name with appending "-c $CONTAINER_NAME"

```sh
kubectl -n $NAMESPACE_NAME exec $OBJECT_NAME $COMMAND
#Sample:
kubectl -n demo1 exec nginx-6db4cdccc6-2r5br "ls"
```

## Object types

> all  
> clusterrolebindings  
> clusterroles  
> cm = configmaps  
> controllerrevisions  
> crd = customresourcedefinition  
> cronjobs  
> cs = componentstatuses  
> csr = certificatesigningrequests  
> deploy = deployments  
> ds = daemonsets  
> ep = endpoints  
> ev = events  
> hpa = horizontalpodautoscalers  
> ing = ingresses  
> jobs  
> limits = limitranges  
> netpol = networkpolicies  
> no = nodes  
> ns = namespaces  
> pdb = poddisruptionbudgets  
> po = pods  
> podpreset  
> podtemplates  
> psp = podsecuritypolicies  
> pv = persistentvolumes  
> pvc = persistentvolumeclaims  
> quota = resourcequotas  
> rc = replicationcontrollers  
> rolebindings  
> roles  
> rs = replicasets  
> sa = serviceaccounts  
> sc = storageclasses  
> secrets  
> sts = statefulsets  