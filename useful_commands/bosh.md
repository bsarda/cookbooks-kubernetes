# Useful commands - BOSH

## Get certificate of BOSH

```sh
om -t https://${OPSMAN_HOST} -u "${OPSMAN_ADMIN}" -p "${OPSMAN_PASSWORD}" -k curl -p /api/v0/certificate_authorities -s | python3 -c '
import sys, json;
jason=json.load(sys.stdin);
for j in jason["certificate_authorities"]:
  if j["active"] == True:
    print(j["cert_pem"])' > ${OPSMAN_CERT_FILE}
```

## Add the environment alias

**Note: when adding alias, ensure the BOSH_CLIENT and BOSH_CLIENT_SECRET are NOT set**  
get the ip address from opsman, clic on the Director tile, tab Status

```sh
unset BOSH_CLIENT
unset BOSH_CLIENT_SECRET
bosh alias-env pks -e $BOSH_HOST --ca-cert $BOSH_CA_CERT
#Sample:
bosh alias-env pks -e 172.16.41.11 --ca-cert /home/ubuntu/pks/opsman-ca.crt
```

## Set the default client, secret, env

(get info from opsman, Director tile, Credentials tab, clic on the "bosh commandline credentials")

```sh
#Sample:
export BOSH_CLIENT=ops_manager
export BOSH_CLIENT_SECRET=UDia03geUHv7l0k_RWY_2pxLbKbmgt3k
export BOSH_CA_CERT=/home/ubuntu/bosh-cert.crt
export BOSH_ENVIRONMENT=172.16.12.7
```

## Show BOSH managed VMs without the env variables set

```sh
bosh -e $BOSH_HOST --client $BOSH_CLIENT_NAME --client-secret=$BOSH_CLIENT_SECRET --ca-cert $BOSH_CA_CERT vms
#Sample:
bosh -e 172.16.41.11 --client ops_manager --client-secret=v2Mouny7nxuFxrF4DXdF-Mv-afYQvM72 --ca-cert /home/ubuntu/pks-deploy/opsman-ca.crt vms
```

## Show BOSH managed VMs

```sh
bosh vms
```

## Show latest 10 recent tasks

```sh
bosh tasks -r10
```

## Describe and follow a task

```sh
bosh task $BOSH_TASK_ID
#Sample:
bosh task 120
```

## Show the debug output of a task (with filter on info)

```sh
bosh task $BOSH_TASK_ID --debug | grep INFO
#Sample:
bosh task 120 --debug | grep INFO
```

## Check and Repair a deployment

```sh
bosh cck -d $DEPLOYMENT_ID cck
#Sample:
bosh cck -d service-instance_45e7ef01-a352-4cd8-9572-b94ba62afdcd
```

## SSH to a node

```sh
bosh -d $DEPLOYMENT_ID ssh $NODE_ID
#Sample:
bosh -d service-instance_45e7ef01-a352-4cd8-9572-b94ba62afdcd ssh worker/1a114587-9a71-4d74-a0fe-6bfd0155f9c3
```

## View the drain errors logs, connected on a node

```sh
sudo -i
ls -l /var/vcap/sys/log/kubelet/
less /var/vcap/sys/log/kubelet/drain.stderr.log
```

## View details of a deployment

"instances" could be also "is"
Details includes state, disk id, agent id, index, resurection, bootstrap

```sh
bosh -d $DEPLOYMENT_ID stop $VM_ID
#Sample:
bosh -d service-instance_45e7ef01-a352-4cd8-9572-b94ba62afdcd instances --details
```

## View usage vitals of a deployment

"instances" could be also "is"
Vitals include loads of cpu, memory, swap, disks

```sh
bosh -d $DEPLOYMENT_ID stop $VM_ID
#Sample:
bosh -d service-instance_45e7ef01-a352-4cd8-9572-b94ba62afdcd instances --vitals
```

## View process of a deployment

"instances" could be also "is"

```sh
bosh -d $DEPLOYMENT_ID stop $VM_ID
#Sample:
bosh -d service-instance_45e7ef01-a352-4cd8-9572-b94ba62afdcd instances --ps
```

## Stop a vm

```sh
bosh -d $DEPLOYMENT_ID stop $VM_ID
#Sample:
bosh -d service-instance_45e7ef01-a352-4cd8-9572-b94ba62afdcd stop worker/0
```

The vm ID could be generalized:

- worker > means all the worker nodes
- master > means all the master nodes
- xxx/# > means the node of type xxx, index # (for example, worker/1). Index starts at 0.

## Stop a deployment (brutal)

```sh
bosh stop --force --hard --skip-drain -d $DEPLOYMENT_ID
#Sample:
bosh stop --force --hard --skip-drain -d service-instance_45e7ef01-a352-4cd8-9572-b94ba62afdcd
```

## Delete a vm from a deployment

```sh
bosh delete-vm -d $DEPLOYMENT_ID $VM_TO_DELETE
#Sample:
bosh delete-vm -d service-instance_45e7ef01-a352-4cd8-9572-b94ba62afdcd vm-3b801d20-b3bc-48fe-a249-4bcbcf1f8a9f
```

## Delete a deployment

Use --force only if needed.

```sh
bosh delete-deployment --force -d $DEPLOYMENT_ID
#Sample:
bosh delete-deployment -d service-instance_45e7ef01-a352-4cd8-9572-b94ba62afdcd
```
