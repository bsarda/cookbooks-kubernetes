# Useful commands - PKS

## Login on pks:

```sh
pks login -a https://$PKS_HOST:9021 -u"$PKS_USER_NAME" -p"$PKS_USER_PASSWORD" -k
#Sample:
pks login -a https://pksapi.corp.local:9021 -u"bsarda" -p"VMware1!" -k
```

## Get the clusters

```sh
pks clusters
```

## Create a cluster

```sh
pks create-cluster $CLUSTER_NAME --external-hostname $CLUSTER_DNS --plan $PKS_PLAN --num-nodes $WORKER_COUNT
#Sample:
pks create-cluster bsa01 --external-hostname bsa01.corp.local --plan small --num-nodes 3
```

## Get the cluster status

While creating, it could be useful to retrieve the cluster status (which give the uuid, used in NSX and BOSH)

```sh
pks cluster $CLUSTER_NAME
#Sample:
pks cluster bsa01
```

## Get creds to use in kubectl

Get the cluster credentials to login on kubectl

```sh
pks get-credentials $CLUSTER_NAME
kubectl config use-context $CLUSTER_NAME
#Sample:
pks get-credentials bsa01
kubectl config use-context bsa01
```

## Show the plans

```sh
pks plans
```

## Network profiles

### Create

create a file, json formated, with the needed parameters.  
list of params could be found there: [PKS Network Profiles define](https://docs.pivotal.io/runtimes/pks/1-3/network-profiles-define.html)  

Sample of a network profile:  

```np_customer_A.json```

```javascript
{
    "name": "np-cust-a",
    "description": "Network Profile for Customer A",
    "parameters": {
        "lb_size": "small",
        "t0_router_id": "405dddbb-313f-4ca0-bb5b-31d9c8b0642c",
        "fip_pool_ids": [
            "d0b2995f-2913-4640-9e09-23983458c0c7"
        ],
        "pod_routable": true,
        "pod_ip_block_ids": [
            "3a89468b-ed27-462c-aac2-1bafa7ec10b7"
        ],
        "master_vms_nsgroup_id": "afa64506-8a61-4c07-8fc1-5e694bc4f6d6",
        "pod_subnet_prefix" : 27
    }
}
```

Then create the profile

```sh
pks create-network-profile np_customer_A.json
```

List the existing network profiles

```sh
pks network-profiles
```

Create a cluster with the network profile

```sh
pks create-cluster $CLUSTER_NAME --external-hostname $CLUSTER_DNS --plan $PKS_PLAN --num-nodes $WORKER_COUNT --network-profile $NETWORK_PROFILE_NAME
#Sample:
pks create-cluster bsa01 --external-hostname bsa01.corp.local --plan small --num-nodes 1 --network-profile np-cust-a
```

## Resize a cluster

```sh
pks resize $CLUSTER_NAME --num-nodes $WORKER_COUNT
#Sample:
pks resize bsa02 --num-nodes 2
```
