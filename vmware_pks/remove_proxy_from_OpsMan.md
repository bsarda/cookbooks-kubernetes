# remove proxy from OpsMan

## Get the private key

retrieve the ssh key in Enterprise Management Console

Go to [https://<pks-ent-mgmt-console>/management/deployment-metadata]  
Get the rsa key of entry "Ops Manager VM SSH Private Key"  
Save it in a file, e.g. `opsman.rsa`

## Connect to OpsMan

Use ssh with the key

```
ssh -i opsman.rsa ubuntu@<opsman-ip>
```

## Remove the proxy entry

```sh
sudo su - tempest-web -s /bin/bash -c 'psql tempest_production'
DELETE from proxy_settings *;
```
