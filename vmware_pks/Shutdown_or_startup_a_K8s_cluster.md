# Shut down or start PKS

source : [KB 67657](https://kb.vmware.com/s/article/67657) and [KB 67656](https://kb.vmware.com/s/article/67656)

## Stop

Get the boh vms to confirm services are running

```sh
bosh -d $DEPLOYMENT_ID instances --ps
#Sample:
bosh -d service-instance_45e7ef01-a352-4cd8-9572-b94ba62afdcd instances --ps
```

Stop the workers

```sh
bosh -d $DEPLOYMENT_ID stop worker
#Sample:
bosh -d service-instance_45e7ef01-a352-4cd8-9572-b94ba62afdcd stop worker
```

Stop the master

```sh
bosh -d $DEPLOYMENT_ID stop master
#Sample:
bosh -d service-instance_45e7ef01-a352-4cd8-9572-b94ba62afdcd stop master
```

Confirm that the status are stopped

```sh
bosh -d $DEPLOYMENT_ID instances --ps
#Sample:
bosh -d service-instance_45e7ef01-a352-4cd8-9572-b94ba62afdcd instances --ps
```

You could then stop all the vms from vSphere (guest shutdown)

## Start

poweron the vms on vSphere.  
do a bosh ssh ton the first master

```sh
bosh -d $DEPLOYMENT_ID ssh master/0
#Sample:
bosh -d service-instance_45e7ef01-a352-4cd8-9572-b94ba62afdcd ssh master/0
```

Start the etcd service and wait for available

```sh
sudo -i
monit start etcd
watch -n 1 monit summary
```

press ctrl+c when it's running.  
Start the second master (to have etcd quorum

```sh
bosh -d $DEPLOYMENT_ID start master/1
#Sample:
bosh -d service-instance_45e7ef01-a352-4cd8-9572-b94ba62afdcd start master/1
```

when its signal are ok, start all the masters

```sh
bosh -d $DEPLOYMENT_ID start master
#Sample:
bosh -d service-instance_45e7ef01-a352-4cd8-9572-b94ba62afdcd start master
```

Confirm that services are up

```sh
bosh -d $DEPLOYMENT_ID instances --ps
#Sample:
bosh -d service-instance_45e7ef01-a352-4cd8-9572-b94ba62afdcd instances --ps
```

start the worker vms

```sh
bosh -d $DEPLOYMENT_ID start worker
#Sample:
bosh -d service-instance_45e7ef01-a352-4cd8-9572-b94ba62afdcd start worker
```

Confirm that services are up

```sh
bosh -d $DEPLOYMENT_ID instances --ps
#Sample:
bosh -d service-instance_45e7ef01-a352-4cd8-9572-b94ba62afdcd instances --ps
```


Confirm the componentstatus of Kubernetes shows all 3 etcd services are Healthy.
kubectl get componentstatus

