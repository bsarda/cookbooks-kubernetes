# How to Create or Remove Custom VM_TYPE Template using the Operations Manager API

## Link

https://community.pivotal.io/s/article/How-to-Create-or-Remove-Custom-VMTYPE-Template-using-the-Ops-Manager-API
Article Number: 3441

## SUMMARY
Summary of the article that will appear along with the Article Title in any search results of the Knowledge Base.
This article applies to Pivotal Cloud Foundry 1.10 and above

## PRODUCT
Pivotal Cloud Foundry Elastic Runtime Suite
There may be a needed to create a custom vm_type as the current predefined templates are not the configuration that a customer wants. A good example can be where the customer has plenty resources and wants to create a VM with high CPU, Disk, and RAM resources.

## CAUSE
Description of what caused the problem.

## PROCEDURE
We have the following pre-defined vm_type templates available currently. The following list is a snapshot taken at the time of writing this article. You should execute Step #3 below to retrieve the list from your system.

```javascript
{
    "vm_types": [
        {
            "name": "Standard_DS1_v2",
            "ram": 3584,
            "cpu": 1,
            "ephemeral_disk": 51200,
            "builtin": true
        },
        {
            "name": "Standard_DS2_v2",
            "ram": 7168,
            "cpu": 2,
            "ephemeral_disk": 102400,
            "builtin": true
        },
        {
            "name": "Standard_DS3_v2",
            "ram": 14336,
            "cpu": 4,
            "ephemeral_disk": 204800,
            "builtin": true
        },
        {
            "name": "Standard_DS4_v2",
            "ram": 28672,
            "cpu": 8,
            "ephemeral_disk": 409600,
            "builtin": true
        },
        {
            "name": "Standard_DS5_v2",
            "ram": 57344,
            "cpu": 8,
            "ephemeral_disk": 819200,
            "builtin": true
        },
        {
            "name": "Standard_DS11_v2",
            "ram": 14336,
            "cpu": 2,
            "ephemeral_disk": 102400,
            "builtin": true
        },
        {
            "name": "Standard_DS12_v2",
            "ram": 28672,
            "cpu": 4,
            "ephemeral_disk": 204800,
            "builtin": true
        },
        {
            "name": "Standard_DS13_v2",
            "ram": 57344,
            "cpu": 8,
            "ephemeral_disk": 409600,
            "builtin": true
        },
        {
            "name": "Standard_DS14_v2",
            "ram": 114688,
            "cpu": 16,
            "ephemeral_disk": 819200,
            "builtin": true
        },
        {
            "name": "Standard_F1s",
            "ram": 2048,
            "cpu": 1,
            "ephemeral_disk": 16384,
            "builtin": true
        },
        {
            "name": "Standard_F2s",
            "ram": 4096,
            "cpu": 2,
            "ephemeral_disk": 32768,
            "builtin": true
        },
        {
            "name": "Standard_F4s",
            "ram": 8192,
            "cpu": 4,
            "ephemeral_disk": 65536,
            "builtin": true
        },
        {
            "name": "Standard_F8s",
            "ram": 16384,
            "cpu": 8,
            "ephemeral_disk": 131072,
            "builtin": true
        }
    ]
}
```

### Create Custom VM_TYPE

Let's consider that we need to create a vm_type with the following specs
CPU: 8, RAM: 64 GB, Disk: 128 GB

As you can see there is no pre-defined vm_type that matches the above specs. Let's follow the steps below to create vm_type: custom type.

1. SSH into Operations Manager VM.

2. Use UAAC command to target your Operations Manager and generate token:

```sh
uaac target https://<OpsMan-Hostname>/uaa
uaac token owner get
Client name: opsman
Client secret: <Leave this Blank>
User name: your_username_here / admin
Password: your_username_here / admin_password
```

In the case of SAML integration, please follow the procedure below:

```sh
uaac token sso get
Client name: opsman
Client secret: leave blank
click on the url and paste in the code below
Password: 
```

3. Use the following command to retrieve the existing pre-loaded vm_types

```sh
uaac curl https://<OpsMan-Hostname>/api/v0/vm_types -k
```

4. Store the output of above command, which is only the value of RESPONSE BODY and not anything else as shown below in blue text:

RESPONSE BODY:
```javascript
{
  "vm_types": [
    {
      "name": "nano",
      "ram": 512,
      "cpu": 1,
      "ephemeral_disk": 1024,
      "builtin": true
    },
   ............. 
   ............. 
   .............
  ]
 }
```

5. Add custom vm_type with the desired specification at the bottom of the above output and use it with the following command:

```sh
uaac curl https://<OpsMan-Hostname>/api/v0/vm_types -X PUT -H "Content-Type: application/json" -d 
 '{
  "vm_types": [
    {
      "name": "nano",
      "ram": 512,
      "cpu": 1,
      "ephemeral_disk": 1024,
      "builtin": true
    },
   ............. 
   ............. 
   .............
   {
      "name": "customtype",
      "cpu": 8,
      "ram": 65536,
      "ephemeral_disk": 131072
   } 
  ]
 }' -k
```

Note: Changes above are immediate and no restart is required, just refresh the browser to see the new list of options.

6. Now you can use this newly added vm_type to replace and update any existing and future VMs.

### Remove Custom VM_TYPE

Use the following commands to remove **all** vm_types:

```sh
uaac curl https://<OpsMan-Hostname>/api/v0/vm_types -X DELETE
```

To bring back default vm_types, do a PUT with the output from step 4).

```sh
uaac curl https://<OpsMan-Hostname>/api/v0/vm_types -X PUT -H "Content-Type: application/json" -d 
'{
"vm_types": [
  {
    "name": "nano",
    "ram": 512,
    "cpu": 1,
    "ephemeral_disk": 1024,
    "builtin": true
  },
 ............. 
 ............. 
 .............
 ]
}' -k
```

### Impact

Please be sure to append custom vm_type values to the existing list otherwise it will replace the entire list of default vm_types.  If you accidentally do this, run the command Remove Custom VM_TYPE to restore the defaults and try again.
Also, if you wrongly remove the entire list and just keep your custom vm_type then the updated list will not take effect until you apply changes.
Once the changes are applied, all the VMs will be updated based on your custom template as this new vm_type will be the only template.
