# Docker harbor login

## Env variables

```sh
HARBOR_HOST=harbor.corp.local
REGISTRY_USER=bsarda
REGISTRY_PASS=VMware1!
REGISTRY_PROJECT=private-book
```

## Create a user

Login to harbor UI, create a user, a project, assign the user to it.

## Download cert

```sh
sudo mkdir -p /etc/docker/certs.d/${HARBOR_HOST}
sudo curl -k -L -u ${REGISTRY_USER}:${REGISTRY_PASS} https://${HARBOR_HOST}/api/systeminfo/getcert -o /etc/docker/certs.d/${HARBOR_HOST}/ca.crt
```

## And now login on it

```sh
sudo docker login ${HARBOR_HOST} -u ${REGISTRY_USER} -p ${REGISTRY_PASS}
```

## Try to get and push

```sh
sudo docker pull nginx
sudo docker tag nginx:latest ${HARBOR_HOST}/${REGISTRY_PROJECT}/nginx:latest
sudo docker push ${HARBOR_HOST}/${REGISTRY_PROJECT}/nginx:latest
```

Login to harbor and check on the UI on the project, you should see the pushed image, and can click on scan.
