# Shut down or start PKS

source : [KB 67657](https://kb.vmware.com/s/article/67657) and [KB 67656](https://kb.vmware.com/s/article/67656)

## Stop

### Shutdown the K8s cluster

Stop the K8s cluster (see the other file _Shutdown or startup a K8s cluster_)  
Stop the vms from vCenter (shutdown guestos)  

### Shutdown the PKS vm

Stop PKS API vm

```sh
bosh -d $DEPLOYMENT_ID stop
#Sample:
bosh -d pivotal-container-service-2512b5929f7e3f581d7e stop
```

then shutdown the vm from vCenter.

### Shutdown the Harbor vm

Stop the Harbor VM by using Bosh:

```sh
bosh -d $DEPLOYMENT_ID stop
#Sample:
bosh -d harbor-container-registry-34be5e9be4561f0f3004 stop
```

Then shutdown the vm from vCenter.  

### Shutdown the remaining

- Shutdown BOSH Director vm from the vCenter.
- Shutdown Opsman vm from the vCenter.
- Shutdown NSX-T managers, then the Edge nodes.
- Put ESXi in maintenance.
- Shutdown the vCenter Server.
- Shutdown the ESXi servers.

## Start

- Poweron the ESXi hosts
- Poweron the vCenter Server
- Exit ESXi from maintenance mode
- Poweron the NSX Manager, the the Edge nodes.
- Poweron the Opsman and unlock it
- Poweron the BOSH Director vm

### If BOSH commands does not work

In case of the bosh commands does not work with something like:

>Fetching info:
>Performing request GET 'https://bosh-ip:25555/info':
>Performing GET request:
>Requesting token via client credentials grant: UAA responded with non-successful status code '503' response 'FAILURE'

Then connect to BOSH vm with SSH (credentials on the tile, tab Credentials) and restart services not monitored or failing.  
Verify the status of services:

```sh
sudo -i
monit summary
```

Restart a service:

```sh
monit restart $SERVICE_NAME
```

### Start PKS vm

Poweron the vm.

If the commands does not work with something like

> Error: Unable to send a request to API: Head https://pks-api_FQDN:9021/v1/clusters: dial tcp pks-api-IP:9021: connect: connection refused"

Then connect to PKS API vm with BOSH (bosh -d ... ssh) and restart services not monitored or failing.  
Verify the status of services:

```sh
sudo -i
monit summary
```

Restart a service:

```sh
monit restart $SERVICE_NAME
```

### Start Harbor

Poweron the Harbor vm.
Verify the status and (re)start services if required.

### Start K8s cluster

Start the vms from vCenter.  
Start the K8s cluster (see the other file _Shutdown or startup a K8s cluster_)  

### Applications

Start all the applications/pods as required.
