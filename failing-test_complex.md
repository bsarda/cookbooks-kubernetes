# Kubernetes Failing test

## Situation

PKS 1.5.1 installed and working. Routed, no NAT.
K8s 1.14.1 cluster: 3 masters (az1, az2, az2), 2 workers (az1, az2).  
1 Deployment of 2 Pods (nginx on namespace demo1), no tags to limit to any az.  

Test of failure:  
Poweroff 2 masters and one worker (that's to see if Pods are restarted)

## Prepare connect

Connect (via bosh) to the master and worker nodes.  
We will kill bosh too, so need to be done **before** killing vms.  

Let's connect to the 3rd (and last) master, and the first worker - that's just for example!

```console
ubuntu@lab1-ubu:~$ bosh -d service-instance_32d64709-201c-4046-8031-e41fc2c7e95d ssh worker/0
```

```console
ubuntu@lab1-ubu:~$ bosh -d service-instance_32d64709-201c-4046-8031-e41fc2c7e95d ssh master/2
```

## Get ETCD keys

Get all the keys which exists:

```console
master/876e2f79-2bfc-4426-927f-68614b34cabd:~$ cd /var/vcap/packages/kubernetes/bin
master/876e2f79-2bfc-4426-927f-68614b34cabd:/var/vcap/packages/kubernetes/bin$ ETCDCTL_API=3 etcdctl get / --prefix --keys-only > /tmp/etcd-keys
```

Test reading some key:  

```console
master/876e2f79-2bfc-4426-927f-68614b34cabd:~$ cd /var/vcap/packages/kubernetes/bin
master/876e2f79-2bfc-4426-927f-68614b34cabd:/var/vcap/packages/kubernetes/bin$ ETCDCTL_API=3 etcdctl get /registry/deployments/demo1/nginx
/registry/deployments/demo1/nginx
{"kind":"Deployment","apiVersion":"apps/v1","metadata":{"name":"nginx","namespace":"demo1","uid":"b999e1bc-0c5d-11ea-bb95-005056870daa","generation":1,"creationTimestamp":"2019-11-21T12:52:07Z","labels":{"app":"nginx"},"annotations":{"deployment.kubernetes.io/revision":"1","kubectl.kubernetes.io/last-applied-configuration":"{\"apiVersion\":\"apps/v1\",\"kind\":\"Deployment\",\"metadata\":{\"annotations\":{},\"labels\":{\"app\":\"nginx\"},\"name\":\"nginx\",\"namespace\":\"demo1\"},\"spec\":{\"replicas\":2,\"selector\":{\"matchLabels\":{\"app\":\"nginx\"}},\"template\":{\"metadata\":{\"labels\":{\"app\":\"nginx\"}},\"spec\":{\"containers\":[{\"image\":\"nginx\",\"name\":\"nginx\",\"ports\":[{\"containerPort\":80}],\"resources\":{\"limits\":{\"cpu\":\"1000m\",\"memory\":\"512Mi\"},\"requests\":{\"cpu\":\"100m\",\"memory\":\"128Mi\"}}}]}}}}\n"}},"spec":{"replicas":2,"selector":{"matchLabels":{"app":"nginx"}},"template":{"metadata":{"creationTimestamp":null,"labels":{"app":"nginx"}},"spec":{"containers":[{"name":"nginx","image":"nginx","ports":[{"containerPort":80,"protocol":"TCP"}],"resources":{"limits":{"cpu":"1","memory":"512Mi"},"requests":{"cpu":"100m","memory":"128Mi"}},"terminationMessagePath":"/dev/termination-log","terminationMessagePolicy":"File","imagePullPolicy":"Always"}],"restartPolicy":"Always","terminationGracePeriodSeconds":30,"dnsPolicy":"ClusterFirst","securityContext":{},"schedulerName":"default-scheduler"}},"strategy":{"type":"RollingUpdate","rollingUpdate":{"maxUnavailable":"25%","maxSurge":"25%"}},"revisionHistoryLimit":10,"progressDeadlineSeconds":600},"status":{"observedGeneration":1,"replicas":2,"updatedReplicas":2,"readyReplicas":2,"availableReplicas":2,"conditions":[{"type":"Progressing","status":"True","lastUpdateTime":"2019-11-21T12:52:36Z","lastTransitionTime":"2019-11-21T12:52:07Z","reason":"NewReplicaSetAvailable","message":"ReplicaSet \"nginx-7db5bb848\" has successfully progressed."},{"type":"Available","status":"True","lastUpdateTime":"2019-11-21T21:25:36Z","lastTransitionTime":"2019-11-21T21:25:36Z","reason":"MinimumReplicasAvailable","message":"Deployment has minimum availability."}]}}

master/876e2f79-2bfc-4426-927f-68614b34cabd:/var/vcap/packages/kubernetes/bin$ ETCDCTL_API=3 etcdctl get /registry/masterleases/5.197.1.4
/registry/masterleases/5.197.1.4
{"kind":"Endpoints","apiVersion":"v1","metadata":{"generation":3090,"creationTimestamp":null},"subsets":[{"addresses":[{"ip":"5.197.1.4"}]}]}

master/876e2f79-2bfc-4426-927f-68614b34cabd:/var/vcap/packages/kubernetes/bin$ ETCDCTL_API=3 etcdctl get /registry/podsecuritypolicy/observability-manager
/registry/podsecuritypolicy/observability-manager
{"kind":"PodSecurityPolicy","apiVersion":"policy/v1beta1","metadata":{"name":"observability-manager","uid":"19cf6a3b-0088-11ea-99c7-005056877809","creationTimestamp":"2019-11-06T11:25:13Z","annotations":{"kubectl.kubernetes.io/last-applied-configuration":"{\"apiVersion\":\"policy/v1beta1\",\"kind\":\"PodSecurityPolicy\",\"metadata\":{\"annotations\":{},\"name\":\"observability-manager\"},\"spec\":{\"fsGroup\":{\"rule\":\"RunAsAny\"},\"runAsUser\":{\"rule\":\"RunAsAny\"},\"seLinux\":{\"rule\":\"RunAsAny\"},\"supplementalGroups\":{\"rule\":\"RunAsAny\"}}}\n"}},"spec":{"seLinux":{"rule":"RunAsAny"},"runAsUser":{"rule":"RunAsAny"},"supplementalGroups":{"rule":"RunAsAny"},"fsGroup":{"rule":"RunAsAny"},"allowPrivilegeEscalation":true}}
```

## Get the Pods to Node

Get the k8s nodes and pods

```console
ubuntu@lab1-ubu:~$ kubectl get no,pods -n demo1 -o wide
NAME                                        STATUS   ROLES    AGE   VERSION   INTERNAL-IP   EXTERNAL-IP   OS-IMAGE             KERNEL-VERSION      CONTAINER-RUNTIME
node/0b19c791-f879-4773-b4a0-d08fcfd84642   Ready    <none>   47m   v1.14.6   5.197.1.5     5.197.1.5     Ubuntu 16.04.6 LTS   4.15.0-64-generic   docker://18.9.8
node/53f43111-b4b2-4a65-887f-d0338c737f79   Ready    <none>   30h   v1.14.6   5.197.1.7     5.197.1.7     Ubuntu 16.04.6 LTS   4.15.0-64-generic   docker://18.9.8

NAME                        READY   STATUS    RESTARTS   AGE   IP          NODE                                   NOMINATED NODE   READINESS GATES
pod/nginx-7db5bb848-dkv26   1/1     Running   1          9h    5.191.1.2   53f43111-b4b2-4a65-887f-d0338c737f79   <none>           <none>
pod/nginx-7db5bb848-n8t84   1/1     Running   1          57m   5.191.1.4   53f43111-b4b2-4a65-887f-d0338c737f79   <none>           <none>
```

Pods are running on node `53f43111-b4b2-4a65-887f-d0338c737f79`  
Get the matching vm bosh id:

```console
ubuntu@lab1-ubu:~$ kubectl describe node/53f43111-b4b2-4a65-887f-d0338c737f79 | grep bosh
                    bosh.id=b151de2e-dbb0-4786-ae34-29a95145ddd2
                    bosh.zone=az2
```

the bosh id of the worker node is `b151de2e-dbb0-4786-ae34-29a95145ddd2`  
and now, match to the vm id:

```console
ubuntu@lab1-ubu:~$ bosh vms
[...]

Deployment 'service-instance_32d64709-201c-4046-8031-e41fc2c7e95d'

Instance                                     Process State  AZ   IPs        VM CID                                   VM Type      Active
master/01b92dd4-f1a4-4a7f-8972-cb3fe67ff94f  running        az2  5.197.1.3  vm-8fabb1b3-bb89-40f3-8a63-8b4f6503ac8e  medium.disk  true
master/7b90cc10-8440-4d13-875b-45b5b7cfa252  running        az3  5.197.1.4  vm-d1e3f09f-f3b7-407c-8b66-615ac2304e3b  medium.disk  true
master/876e2f79-2bfc-4426-927f-68614b34cabd  running        az1  5.197.1.2  vm-6c082580-1504-4b7c-a05e-158d8471300e  medium.disk  true
worker/26512f08-d2da-4b3b-95ab-8b326e52961a  running        az1  5.197.1.5  vm-87f45307-1264-4c1f-bf7b-e59f4e6adf80  medium.disk  true
worker/b151de2e-dbb0-4786-ae34-29a95145ddd2  running        az2  5.197.1.7  vm-ef8c81fa-eb75-462f-8ab9-2d4e127fedb3  medium.disk  true
```

The vm name is `vm-ef8c81fa-eb75-462f-8ab9-2d4e127fedb3`

## Check on the nodes

SSH to the worker nodes  
Get the current running docker containers:  

```console
docker ps | grep nginx-7db5bb848
```

## Power off the vms to simulate failure

Power off the vms except:

- vm-6c082580-1504-4b7c-a05e-158d8471300e (the master node)
- vm-87f45307-1264-4c1f-bf7b-e59f4e6adf80 (the worker node which must survive)
- vm-c0d13a17-90c6-48a2-8776-a839701923d3 (the harbor repo)

## Tests

### On the remaining master

```console
master/876e2f79-2bfc-4426-927f-68614b34cabd:~$ ETCDCTL_API=3 etcdctl get /registry/deployments/demo1/nginx
Error: context deadline exceeded
```

```console
master/876e2f79-2bfc-4426-927f-68614b34cabd:~$ tail /var/vcap/sys/log/etcd/etcd.stderr.log -f
2019-11-21 22:32:17.779624 W | etcdserver: read-only range request "key:\"/registry/apiextensions.k8s.io/customresourcedefinitions/\" range_end:\"/registry/apiextensions.k8s.io/customresourcedefinitions0\" " with result "error:etcdserver: request timed out" took too long (10.160558451s) to execute
2019-11-21 22:32:17.779748 W | etcdserver: read-only range request "key:\"/registry/secrets/\" range_end:\"/registry/secrets0\" " with result "error:etcdserver: request timed out" took too long (8.896026213s) to execute
2019-11-21 22:32:17.780418 W | etcdserver: read-only range request "key:\"/registry/pksapi.io/metricsinks\" range_end:\"/registry/pksapi.io/metricsinkt\" count_only:true " with result "error:etcdserver: request timed out" took too long (10.247794139s) to execute
2019-11-21 22:32:18.336480 W | rafthttp: health check for peer 3f6ce6a13fa41036 could not connect: dial tcp 5.197.1.4:2380: i/o timeout (prober "ROUND_TRIPPER_RAFT_MESSAGE")
2019-11-21 22:32:18.418903 W | rafthttp: health check for peer 436e08a8c9a9ed02 could not connect: dial tcp 5.197.1.3:2380: i/o timeout (prober "ROUND_TRIPPER_RAFT_MESSAGE")
2019-11-21 22:32:18.905982 I | raft: 17f206fd866fdab2 is starting a new election at term 980
2019-11-21 22:32:18.906059 I | raft: 17f206fd866fdab2 became candidate at term 981
2019-11-21 22:32:18.906083 I | raft: 17f206fd866fdab2 received MsgVoteResp from 17f206fd866fdab2 at term 981
2019-11-21 22:32:18.906101 I | raft: 17f206fd866fdab2 [logterm: 733, index: 2502335] sent MsgVote request to 3f6ce6a13fa41036 at term 981
2019-11-21 22:32:18.906116 I | raft: 17f206fd866fdab2 [logterm: 733, index: 2502335] sent MsgVote request to 436e08a8c9a9ed02 at term 981
```

```console
master/876e2f79-2bfc-4426-927f-68614b34cabd:~$ tail /var/vcap/sys/log/kube-apiserver/kube-apiserver.stderr.log -f
I1121 22:34:18.996068       7 asm_amd64.s:1337] balancerWrapper: got update addr from Notify: [{master-1.etcd.cfcr.internal:2379 <nil>} {master-2.etcd.cfcr.internal:2379 <nil>}]
W1121 22:34:18.996636       7 asm_amd64.s:1337] Failed to dial master-1.etcd.cfcr.internal:2379: grpc: the connection is closing; please retry.
W1121 22:34:19.009230       7 clientconn.go:1251] grpc: addrConn.createTransport failed to connect to {master-2.etcd.cfcr.internal:2379 0  <nil>}. Err :connection error: desc = "transport: Error while dialing dial tcp 5.197.1.4:2379: connect: no route to host". Reconnecting...
```

### On the remaining worker node

```console
docker ps | grep nginx-7db5bb848
```

Nothing is shown :(  

```console
worker/26512f08-d2da-4b3b-95ab-8b326e52961a:~$ tail /var/vcap/sys/log/kubelet/kubelet.stderr.log -f
E1121 22:44:04.911524    3179 reflector.go:126] k8s.io/client-go/informers/factory.go:133: Failed to list *v1beta1.CSIDriver: Get https://master.cfcr.internal:8443/apis/storage.k8s.io/v1beta1/csidrivers?limit=500&resourceVersion=0: read tcp 5.197.1.5:55622->5.197.1.2:8443: use of closed network connection
E1121 22:44:04.911602    3179 reflector.go:126] object-"pks-system"/"pks-ca": Failed to list *v1.Secret: Get https://master.cfcr.internal:8443/api/v1/namespaces/pks-system/secrets?fieldSelector=metadata.name%3Dpks-ca&limit=500&resourceVersion=0: read tcp 5.197.1.5:55622->5.197.1.2:8443: use of closed network connection
E1121 22:44:04.912683    3179 reflector.go:126] object-"pks-system"/"telegraf": Failed to list *v1.ConfigMap: Get https://master.cfcr.internal:8443/api/v1/namespaces/pks-system/configmaps?fieldSelector=metadata.name%3Dtelegraf&limit=500&resourceVersion=0: read tcp 5.197.1.5:55622->5.197.1.2:8443: use of closed network connection
```

kubelet ctl:

```console
worker/26512f08-d2da-4b3b-95ab-8b326e52961a:~$ tail /var/vcap/sys/log/kubelet/kubelet_ctl.stderr.log -f
+ node_name=
+ [[ ! -z '' ]]
+ start_kubelet
+ ln -s -f /var/vcap/jobs/kubelet/packages/cni/bin/nsenter /usr/bin/nsenter
++ get_hostname_override
++ [[ gce == \v\s\p\h\e\r\e ]]
++ [[ azure == \v\s\p\h\e\r\e ]]
++ hostname_override=5.197.1.5
++ echo 5.197.1.5
+ kubelet --cni-bin-dir=/var/vcap/jobs/kubelet/packages/cni/bin --container-runtime=docker --docker=unix:///var/vcap/sys/run/docker/docker.sock --docker-endpoint=unix:///var/vcap/sys/run/docker/docker.sock --kubeconfig=/var/vcap/jobs/kubelet/config/kubeconfig --network-plugin=cni --pod-infra-container-image=vmware/pause:3.1 --root-dir=/var/vcap/data/kubelet --cloud-provider=vsphere --hostname-override=5.197.1.5 --node-labels=pks-system/cluster.name=bsa02,pks-system/cluster.uuid=service-instance_32d64709-201c-4046-8031-e41fc2c7e95d,spec.ip=5.197.1.5,bosh.id=26512f08-d2da-4b3b-95ab-8b326e52961a,bosh.zone=az1,failure-domain.beta.kubernetes.io/zone=az1 --config=/var/vcap/jobs/kubelet/config/kubeletconfig.yml
```

20 minutes after the failure...  

```console
docker ps | grep nginx-7db5bb848
```

still shows nothing :/  

```console
worker/26512f08-d2da-4b3b-95ab-8b326e52961a:~$ cat /var/vcap/sys/log/kubelet/kubelet.stderr.log | grep nginx
W1121 21:34:37.999865    9916 docker_sandbox.go:384] failed to read pod IP from plugin/docker: NetworkPlugin cni failed on the status hook for pod "nginx-7db5bb848-5kd9m_demo1": CNI failed to retrieve network namespace path: cannot find network namespace for the terminated container "0f467dc620fc2e601e43a074cf96141279071c6c0d56b2ec29e816b6b97c3b12"
W1121 21:35:49.391219    9916 docker_sandbox.go:384] failed to read pod IP from plugin/docker: NetworkPlugin cni failed on the status hook for pod "nginx-7db5bb848-5kd9m_demo1": CNI failed to retrieve network namespace path: cannot find network namespace for the terminated container "0f467dc620fc2e601e43a074cf96141279071c6c0d56b2ec29e816b6b97c3b12"
```

Find in multiple kubelet logs

```console
grep nginx /var/vcap/sys/log/kubelet/*
```

Find in ALL the logs (in case we missed it?!)

```console
worker/26512f08-d2da-4b3b-95ab-8b326e52961a:~$ find /var/vcap/sys/log/ -type f -exec grep 'nginx-7db5bb848' {} \;
W1121 21:34:37.999865    9916 docker_sandbox.go:384] failed to read pod IP from plugin/docker: NetworkPlugin cni failed on the status hook for pod "nginx-7db5bb848-5kd9m_demo1": CNI failed to retrieve network namespace path: cannot find network namespace for the terminated container "0f467dc620fc2e601e43a074cf96141279071c6c0d56b2ec29e816b6b97c3b12"
W1121 21:35:49.391219    9916 docker_sandbox.go:384] failed to read pod IP from plugin/docker: NetworkPlugin cni failed on the status hook for pod "nginx-7db5bb848-5kd9m_demo1": CNI failed to retrieve network namespace path: cannot find network namespace for the terminated container "0f467dc620fc2e601e43a074cf96141279071c6c0d56b2ec29e816b6b97c3b12"
```

and even 30 minutes after:

```console
ubuntu@lab1-ubu:~$ worker/26512f08-d2da-4b3b-95ab-8b326e52961a:~# docker ps
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS               NAMES
cb1112289b16        014871c36f12        "ghostunnel server -…"   39 minutes ago      Up 39 minutes                           k8s_ghostunnel_fluent-bit-wt52p_pks-system_73b73577-0cac-11ea-b99c-005056870daa_0
aae8a8ca3dda        9dcd92233333        "/fluent-bit/bin/flu…"   39 minutes ago      Up 39 minutes                           k8s_fluent-bit_fluent-bit-wt52p_pks-system_73b73577-0cac-11ea-b99c-005056870daa_0
2e7a4b0ff0c7        vmware/pause:3.1    "/pause"                 39 minutes ago      Up 39 minutes                           k8s_POD_fluent-bit-wt52p_pks-system_73b73577-0cac-11ea-b99c-005056870daa_0
88663cde794e        99079cbd0241        "/srv/validator"         39 minutes ago      Up 39 minutes                           k8s_validator_validator-646dc57cdb-9zcsm_pks-system_69c8cd74-0cac-11ea-b99c-005056870daa_0
dfde0fba054c        8d5ba02983ec        "/srv/metric-control…"   39 minutes ago      Up 39 minutes                           k8s_metric-controller_metric-controller-8f67b7868-5fh97_pks-system_6752c646-0cac-11ea-b99c-005056870daa_0
23f06ef841d7        vmware/pause:3.1    "/pause"                 39 minutes ago      Up 39 minutes                           k8s_POD_validator-646dc57cdb-9zcsm_pks-system_69c8cd74-0cac-11ea-b99c-005056870daa_0
12e26f914589        464189a10011        "telegraf --config-d…"   39 minutes ago      Up 39 minutes                           k8s_telegraf_telegraf-86r85_pks-system_67cc8192-0cac-11ea-b99c-005056870daa_0
d5347dfccfb3        014871c36f12        "ghostunnel client -…"   39 minutes ago      Up 39 minutes                           k8s_ghostunnel_event-controller-7f4dcc9bb7-pb7h2_pks-system_640b1f39-0cac-11ea-b99c-005056870daa_0
01c6a54614a9        vmware/pause:3.1    "/pause"                 39 minutes ago      Up 39 minutes                           k8s_POD_telegraf-86r85_pks-system_67cc8192-0cac-11ea-b99c-005056870daa_0
6612990f2c05        vmware/pause:3.1    "/pause"                 39 minutes ago      Up 39 minutes                           k8s_POD_metric-controller-8f67b7868-5fh97_pks-system_6752c646-0cac-11ea-b99c-005056870daa_0
9e086bc867e8        788a84acb393        "/srv/event-controll…"   39 minutes ago      Up 39 minutes                           k8s_event-controller_event-controller-7f4dcc9bb7-pb7h2_pks-system_640b1f39-0cac-11ea-b99c-005056870daa_0
d9049f7b02b8        0affa66c41ae        "/srv/sink-controller"   39 minutes ago      Up 39 minutes                           k8s_sink-controller_sink-controller-774fb78bbd-vkbhr_pks-system_642d006b-0cac-11ea-b99c-005056870daa_0
09cea51df0eb        vmware/pause:3.1    "/pause"                 39 minutes ago      Up 39 minutes                           k8s_POD_sink-controller-774fb78bbd-vkbhr_pks-system_642d006b-0cac-11ea-b99c-005056870daa_0
5dafdd779568        vmware/pause:3.1    "/pause"                 39 minutes ago      Up 39 minutes                           k8s_POD_event-controller-7f4dcc9bb7-pb7h2_pks-system_640b1f39-0cac-11ea-b99c-005056870daa_0
9d91c5ae5229        aed473fef28b        "/usr/bin/cadvisor -…"   41 minutes ago      Up 41 minutes                           k8s_vrops-cadvisor_vrops-cadvisor-6v452_pks-system_bde122d9-0ca6-11ea-bb95-005056870daa_1
a5999fef3fa0        vmware/pause:3.1    "/pause"                 42 minutes ago      Up 41 minutes                           k8s_POD_vrops-cadvisor-6v452_pks-system_bde122d9-0ca6-11ea-bb95-005056870daa_1
```

The fail to recover comes from the fact that Kubelet wants to read in the etcd database, which is broken.  
Reading from the worker:  

```console
worker/26512f08-d2da-4b3b-95ab-8b326e52961a:~$ cat /var/vcap/sys/log/kubelet/kubelet.stderr.log | grep "Failed to list \*v1."
E1121 22:12:28.809974    3179 reflector.go:126] k8s.io/kubernetes/pkg/kubelet/kubelet.go:442: Failed to list *v1.Service: Get https://master.cfcr.internal:8443/api/v1/services?limit=500&resourceVersion=0: dial tcp: lookup master.cfcr.internal on 172.16.10.5:53: no such host
E1121 22:12:28.814516    3179 reflector.go:126] k8s.io/kubernetes/pkg/kubelet/config/apiserver.go:47: Failed to list *v1.Pod: Get https://
E1121 22:12:28.814642    3179 reflector.go:126] k8s.io/kubernetes/pkg/kubelet/kubelet.go:451: Failed to list *v1.Node: Get https://master.cfcr.internal:8443/api/v1/nodes?fieldSelector=metadata.name%3D0b19c791-f879-4773-b4a0-d08fcfd84642&limit=500&resourceVersion=0: dial tcp: lookup master.cfcr.internal on 172.16.10.5:53: no such host
E1124 14:58:41.597880    3179 reflector.go:126] object-"pks-system"/"telegraf": Failed to list *v1.ConfigMap: Get https://master.cfcr.internal:8443/api/v1/namespaces/pks-system/configmaps?fieldSelector=metadata.name%3Dtelegraf&limit=500&resourceVersion=0: read tcp 5.197.1.5:35966->5.197.1.2:8443: use of closed network connection
E1124 14:58:41.598164    3179 reflector.go:126] object-"pks-system"/"validator": Failed to list *v1.Secret: Get https://master.cfcr.internal:8443/api/v1/namespaces/pks-system/secrets?fieldSelector=metadata.name%3Dvalidator&limit=500&resourceVersion=0: read tcp 5.197.1.5:35966->5.197.1.2:8443: use of closed network connection
E1124 14:58:41.599456    3179 reflector.go:126] k8s.io/client-go/informers/factory.go:133: Failed to list *v1beta1.CSIDriver: Get https://master.cfcr.internal:8443/apis/storage.k8s.io/v1beta1/csidrivers?limit=500&resourceVersion=0: read tcp 5.197.1.5:35966->5.197.1.2:8443: use of closed network connection
```

And from the master node:

```console
master/876e2f79-2bfc-4426-927f-68614b34cabd:~$ cat /var/vcap/sys/log/etcd/etcd.stderr.log
2019-11-24 15:06:58.651671 W | etcdserver: read-only range request "key:\"/registry/services/endpoints/kube-system/kube-controller-manager\" " w
ith result "error:context canceled" took too long (9.997735379s) to execute
2019-11-24 15:06:58.815042 W | rafthttp: health check for peer 436e08a8c9a9ed02 could not connect: dial tcp 5.197.1.3:2380: i/o timeout (prober
"ROUND_TRIPPER_RAFT_MESSAGE")
```

## Get'hem back to life

Re-poweron the vms (masters and workers).  
Wait few time (2-3 minutes) and try a kubectl again.  

```console
ubuntu@lab1-ubu:~$ kubectl get pods -A -o wide
NAMESPACE     NAME                                    READY   STATUS    RESTARTS   AGE     IP           NODE                                   NOMINATED NODE   READINESS GATES
demo1         nginx-7db5bb848-dkv26                   0/1     Error     1          3d2h    <none>       53f43111-b4b2-4a65-887f-d0338c737f79   <none>           <none>
demo1         nginx-7db5bb848-n8t84                   0/1     Error     1          2d17h   <none>       53f43111-b4b2-4a65-887f-d0338c737f79   <none>           <none>
kube-system   coredns-95489c5c9-9x2fm                 0/1     Error     1          2d17h   <none>       53f43111-b4b2-4a65-887f-d0338c737f79   <none>           <none>
kube-system   coredns-95489c5c9-cj8ps                 0/1     Error     1          2d17h   <none>       53f43111-b4b2-4a65-887f-d0338c737f79   <none>           <none>
kube-system   coredns-95489c5c9-lvfw7                 0/1     Error     1          2d17h   <none>       53f43111-b4b2-4a65-887f-d0338c737f79   <none>           <none>
kube-system   kubernetes-dashboard-558689fc66-szmg2   0/1     Error     1          2d17h   <none>       53f43111-b4b2-4a65-887f-d0338c737f79   <none>           <none>
kube-system   metrics-server-867b8fdb7d-wjsbb         0/1     Error     1          2d17h   <none>       53f43111-b4b2-4a65-887f-d0338c737f79   <none>           <none>
pks-system    event-controller-7f4dcc9bb7-pb7h2       2/2     Running   0          2d16h   5.191.9.6    0b19c791-f879-4773-b4a0-d08fcfd84642   <none>           <none>
pks-system    fluent-bit-khjxt                        0/2     Error     0          2d16h   <none>       53f43111-b4b2-4a65-887f-d0338c737f79   <none>           <none>
pks-system    fluent-bit-wt52p                        2/2     Running   0          2d16h   5.191.9.17   0b19c791-f879-4773-b4a0-d08fcfd84642   <none>           <none>
pks-system    metric-controller-8f67b7868-5fh97       1/1     Running   0          2d16h   5.191.9.14   0b19c791-f879-4773-b4a0-d08fcfd84642   <none>           <none>
pks-system    observability-manager-d777c7d74-s2f79   0/1     Error     1          2d17h   <none>       53f43111-b4b2-4a65-887f-d0338c737f79   <none>           <none>
pks-system    sink-controller-774fb78bbd-vkbhr        1/1     Running   0          2d16h   5.191.9.7    0b19c791-f879-4773-b4a0-d08fcfd84642   <none>           <none>
pks-system    telegraf-4qdsp                          1/1     Running   1          2d16h   5.197.1.7    53f43111-b4b2-4a65-887f-d0338c737f79   <none>           <none>
pks-system    telegraf-86r85                          1/1     Running   0          2d16h   5.197.1.5    0b19c791-f879-4773-b4a0-d08fcfd84642   <none>           <none>
pks-system    telemetry-agent-858446f4ff-6jzk9        0/2     Error     2          3d23h   <none>       53f43111-b4b2-4a65-887f-d0338c737f79   <none>           <none>
pks-system    validator-646dc57cdb-9zcsm              1/1     Running   0          2d16h   5.191.9.15   0b19c791-f879-4773-b4a0-d08fcfd84642   <none>           <none>
pks-system    vrops-cadvisor-6v452                    1/1     Running   1          2d17h   5.197.1.5    0b19c791-f879-4773-b4a0-d08fcfd84642   <none>           <none>
pks-system    vrops-cadvisor-wslx5                    1/1     Running   2          3d23h   5.197.1.7    53f43111-b4b2-4a65-887f-d0338c737f79   <none>           <none>
```

Obviously, every Pods where on the failed worker are in state error.  
Re-do the same command minutes after:

```console
ubuntu@lab1-ubu:~$ kubectl get pods -A -o wide
NAMESPACE     NAME                                    READY   STATUS    RESTARTS   AGE     IP           NODE                                   NOMINATED NODE   READINESS GATES
demo1         nginx-7db5bb848-dkv26                   1/1     Running   2          3d2h    5.191.1.2    53f43111-b4b2-4a65-887f-d0338c737f79   <none>           <none>
demo1         nginx-7db5bb848-n8t84                   1/1     Running   2          2d18h   5.191.1.4    53f43111-b4b2-4a65-887f-d0338c737f79   <none>           <none>
kube-system   coredns-95489c5c9-9x2fm                 1/1     Running   2          2d18h   5.191.5.5    53f43111-b4b2-4a65-887f-d0338c737f79   <none>           <none>
kube-system   coredns-95489c5c9-cj8ps                 1/1     Running   2          2d18h   5.191.5.3    53f43111-b4b2-4a65-887f-d0338c737f79   <none>           <none>
kube-system   coredns-95489c5c9-lvfw7                 1/1     Running   2          2d18h   5.191.5.6    53f43111-b4b2-4a65-887f-d0338c737f79   <none>           <none>
kube-system   kubernetes-dashboard-558689fc66-szmg2   1/1     Running   2          2d18h   5.191.5.2    53f43111-b4b2-4a65-887f-d0338c737f79   <none>           <none>
kube-system   metrics-server-867b8fdb7d-wjsbb         1/1     Running   2          2d18h   5.191.5.4    53f43111-b4b2-4a65-887f-d0338c737f79   <none>           <none>
pks-system    event-controller-7f4dcc9bb7-5xr5w       2/2     Running   0          13m     5.191.9.2    0b19c791-f879-4773-b4a0-d08fcfd84642   <none>           <none>
pks-system    fluent-bit-5gclp                        2/2     Running   0          13m     5.191.9.12   0b19c791-f879-4773-b4a0-d08fcfd84642   <none>           <none>
pks-system    fluent-bit-vwsmr                        2/2     Running   0          13m     5.191.9.13   53f43111-b4b2-4a65-887f-d0338c737f79   <none>           <none>
pks-system    metric-controller-8f67b7868-z624k       1/1     Running   0          13m     5.191.9.11   0b19c791-f879-4773-b4a0-d08fcfd84642   <none>           <none>
pks-system    observability-manager-d777c7d74-s2f79   1/1     Running   2          2d18h   5.191.9.10   53f43111-b4b2-4a65-887f-d0338c737f79   <none>           <none>
pks-system    sink-controller-774fb78bbd-zslvg        1/1     Running   0          13m     5.191.9.3    0b19c791-f879-4773-b4a0-d08fcfd84642   <none>           <none>
pks-system    telegraf-9zbrd                          1/1     Running   0          13m     5.197.1.7    53f43111-b4b2-4a65-887f-d0338c737f79   <none>           <none>
pks-system    telegraf-vlmfz                          1/1     Running   0          13m     5.197.1.5    0b19c791-f879-4773-b4a0-d08fcfd84642   <none>           <none>
pks-system    telemetry-agent-858446f4ff-6jzk9        2/2     Running   4          3d23h   5.191.9.4    53f43111-b4b2-4a65-887f-d0338c737f79   <none>           <none>
pks-system    validator-646dc57cdb-mm8pv              1/1     Running   0          13m     5.191.9.18   0b19c791-f879-4773-b4a0-d08fcfd84642   <none>           <none>
pks-system    vrops-cadvisor-6v452                    1/1     Running   1          2d17h   5.197.1.5    0b19c791-f879-4773-b4a0-d08fcfd84642   <none>           <none>
pks-system    vrops-cadvisor-wslx5                    1/1     Running   2          3d23h   5.197.1.7    53f43111-b4b2-4a65-887f-d0338c737f79   <none>           <none>
```

Everything seems up again. The services are working, Pods too, configMap are there...  

## Conclusion

What is down finally ?  
The **etcd** database is failing (not either in r/o, but not accessible at all). Because **etcd** is down, **kube-apiserver** service sends timeout or disconnect.  
As the **kube-apiserver** is not responsive, the **kubelet** on each node can't connect to it to get the status. And the _Pods_ can't be restarted because no info can't be retrieved.  

Another side effect - might be the most difficult, is because the management is down, the _Secrets_ and _ConfigMaps_ can't be read, for example. And what is using those constructs? the monitoring tools (with _Secrets_ for storing tokens), and **core-dns** which "only" provides the name resolution of all the _Pods_.  
Because the essence of Kubernetes is to create microservices apps, the _Pods_ and their _Services_ usually communicate together through their names, reverse-matched when using the _selectors_. Resulting that the pieces of the app are not able to communicate.

Because this is not sufficient enough, in the previous section we said that the _Secrets_ are used by monitoring tools, but also by the Ingress controllers and encryption overlays (like Portworx).  
The monitoring will not be able to connect to the K8s cluster, hence will not be able to know the status and health of any K8s construct within the cluster.  
A monitoring which will become totally bind on what is happening in the cluster :/ ... not so good, right ?



portworx status?
helm/tiller?
prometheus?


With 2 master failed out of 3, the whole cluster and its workloads were completely down.  
Why this statement ? well, the Pods on the remaining hosts were up and running, **BUT** because a Pod alone is not sufficient, then we could consider the service is down.  

And because of everything is failing (including reading the secrets, ...), ALL the monitoring which uses K8s constructs will fail too (like the cadvisor or the telegraf)
